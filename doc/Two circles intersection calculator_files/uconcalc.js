var kilo = 0.45359237;
var set0 = 0.00000000000001;

function buildtable_VF(name, frm, col, bgcolor, header, txtWidth, anchore) {
    //newer version includes form name 01-2011
    var tab = "";
    var m = 0;
    var len = name.length;
    var dow = (len - (len % (col + 1))) / (col + 1);
    var row = Math.ceil(len / (col + 1)) - 1;
    var col1 = col + 1;
    var txtIndex;

    tab = "<a name='" + anchore + "'></a>";
    tab += "<form name='" + frm + "'>";
    tab += "<table style='border:1px;background-color:" + bgcolor + "; font-family:Tahoma;font-size:10pt;'>";
    tab += "<tr><td bgcolor='#fef4fe' colspan='" + col1.toString() + "' style='font-size:12pt;color:#1f1f7f;'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp<input type='button' name='dec' value='  -0.0   '  onclick='incMe(-1,this.form.name);' />&nbsp<input type='button' name='inc' value=' +0.00 ' onclick='incMe(1,this.form.name);' />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp <b>" + header + "</b></td></tr>";

    for (j = 0; j <= row; j++) {
        tab += "<tr>";
        for (i = 0; i <= col; i++) {
            tab += "<td width='395px'>";
            if (len > m) {
                txtIndex = j + i * (row + 1);

                tab += "<input type='text' id='f" + txtIndex.toString() + "'";
                tab += " name='f" + txtIndex + "'";
                tab += " style='color:black; width:" + txtWidth.toString() + "px;'";
                tab += " onkeyup='return calculate(this.name, this.value, this.form.name)' />  " + name[txtIndex];
            }
            tab += "</td>";
            m++;
        }
        tab += "</tr>";
    }

    tab += "</table>";
    tab += "</form>";

    return (tab);
}

function buildtable_V(name,col,bgcolor,header,txtWidth){
	var tab = "";
	var m = 0;
	var len = name.length;
	var dow = (len-(len%(col+1)))/(col+1);
	var row = Math.ceil(len / (col + 1)) - 1;
	var col1 = col + 1;
	var txtIndex;
	
	tab += "<table border='1px' bgcolor='" + bgcolor + "' style='font-family:Tahoma;font-size:10pt;'>";
	tab += "<tr><td bgcolor='#fef4fe' colspan='" + col1.toString() + "' style='font-size:12pt;color:#1f1f7f;'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp<input type='button' name='dec' value='  -0.0   '  onclick='dec();' />&nbsp<input type='button' name='inc' value=' +0.00 ' onclick='inc();' />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp <b>" + header + "</b></td></tr>";
	
	for (j=0; j<=row; j++){
		tab += "<tr>";
		for (i=0; i<=col; i++){
			tab += "<td width='395px'>";
			if (len > m){
				txtIndex = j + i*(row+1);
				
				tab += "<input type='text' id='f" + txtIndex.toString() + "'";
				tab += " name='f" + txtIndex + "'";
				tab += " style='color:black; width:" + txtWidth.toString() + "px;'";
				tab += " onKeyUp='return calculate(this.name,this.value)' />  " + name[txtIndex];
			}
			tab += "</td>";
			m++;
		}
		tab += "</tr>";
	}

	tab += "</table>";

	return (tab);
}


function buildtable_VB(name,col,bgcolor,header,txtWidth){
	//Build table vertically without acurracy buttons
	var tab = "";
	var m = 0;
	var len = name.length;
	var dow = (len-(len%(col+1)))/(col+1);
	var row = Math.floor(len/(col+1))-1;
	var col1 = col + 1;
	var txtIndex = 0;
	
	tab += "<table border='1px' bgcolor='" + bgcolor + "' style='font-family:Tahoma;font-size:10pt;'>";
	tab += "<tr><td bgcolor='#fef4fe' colspan='" + col1.toString() + "' style='font-size:12pt;color:#1f1f7f;text-align:center;'><b>" + header + "</b></td></tr>";
	
	for (j=0; j<=row; j++){
		tab += "<tr>";
		for (i=0; i<=col; i++){
			tab += "<td width='395px'>";
			if (len > m){
				txtIndex = j + i*(row+1);
				
				tab += "<input type='text' id='f" + txtIndex.toString() + "'";
				tab += " name='f" + txtIndex.toString() + "'";
				tab += " style='color:black; width:" + txtWidth.toString() + "px;'";
				tab += " onKeyUp='return calculate(this.name,this.value);' />  " + name[txtIndex];
			}
			tab += "</td>";
			m++;
		}
		tab += "</tr>";
	}

	tab += "</table>";
	return (tab);
}


function buildtable(name, col, bgcolor, header, txtWidth) {
	var tab = "";
	var m = 0;
	var len = name.length;
	//var row = (len - (len % (col + 1))) / (col + 1);
	var row = Math.floor((len + 1) / 2) - 1;
	var col1 = col + 1;
	
	tab += "<table border='1px' bgcolor='" + bgcolor + "' style='font-family:Tahoma;font-size:10pt;'>";
	tab += "<tr><td bgcolor='#fef4fe' colspan='" + col1.toString() + "' style='font-size:12pt;color:#1f1f7f;'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp<input type='button' name='dec' value='  -0.0   '  onclick='dec();' />&nbsp<input type='button' name='inc' value=' +0.00 ' onclick='inc();' />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp <b>" + header + "</b></td></tr>";
	
	for (j=0; j<=row; j++){
		tab += "<tr>";
		for (i=0; i<=col; i++){
			tab += "<td width='395px'>";
			if (len > m){
				tab += "<input type='text' id='f" + m.toString() + "'"; 
				tab += " name='f" + m.toString() + "'";
				tab += " style='color:black; width:" + txtWidth.toString() + "px;'";
				tab += " onKeyUp='return calculate(this.name,this.value);' />  " + name[m];
			}
			tab += "</td>";
			m++;
		}
		tab += "</tr>";
	}

	tab += "</table>";

	return (tab);
}

function BuildTrigTable(frm,formname,nam,img,hi,rad,head,hili,hico,anchor,width)
{
	var str;
	var k=0;
	var tot=nam.length-1;
	var nhi=hili.length-1;
	
	str="<a name=" + anchor + "></a>";
	str="<form name='" + frm + "'>";
	str+="<div style='width:650px;border:4px ridge #fef4f0;background-color:#ffffff;font-family:tahoma;font-size:10pt;'>";
	str+="<div style='height:25px;text-align:center;border-bottom:4px ridge #fef4f0;background-color:#c1cdc1;padding-top:3px;'>Trigonometric calculator <b>[" + formname + "]</b></div>";
	str+="<div style='float:left;width:315px;height:" + hi + "px;text-align:center;'><img src='" + img + "' align='middle' />";
	str+="<br /><font size='2'>" + head + "</font><br />";
	
	if (rad==1){
		str+="<input type='radio' name='d9rad' value='rad' onclick='degrad(this.form.name)' checked>Degree";
		str+="<input type='radio' name='d9rad' value='deg' onclick='degrad(this.form.name)'>Radian";
		str+="&nbsp;&nbsp;&nbsp<input type='button' name='incD9clear' value='Clear All' onclick='clearAll(this.form.name,1);' /><br />";
	}
	
	str+="<input type='button' name='incdec' value='0.00-' onclick='dec(this.form.name);' />";
	str+="<input type='button' name='incdec' value='0.00+' onclick='inc(this.form.name);' /></div>";
	str+="<div style='float:left;width:310px;'>";
	str+="<table border='1px' width='335px' style='font-family:tahoma;font-size:10pt;'>";
	for (i=0; i<=tot; i++){
		if (hili[0]==999) 
			str+="<tr><td><input type='text' width='" + width + "px' id='f" + i.toString() + "'";
		else{
			if (hili[k]==i){
				str+="<tr><td bgcolor='" + hico + "'><input type='text' width='" + width + "px' disabled id='f" + i.toString() + "'";
				k++;
			}
			else
				str+="<tr><td><input type='text' width='" + width + "px' id='f" + i.toString() + "'";
		}	
		str+=" name='f" + i.toString() + "' size='16' onkeyup='calc(this.form.name,this.name,this.value);' /> ";
		str+=name[i] +"</td></tr>";
	}	
	str+="</table>";
	str+="</div></div></form>";
	
	return str;
}

function BuildTrigTableWidth(frm, formname, nam, img, hi, rad, head, hili, hico, anchor, width,TextWidth,DivWidth) {
    var str;
    var k = 0;
    var tot = nam.length - 1;
    var nhi = hili.length - 1;

    str = "<a name=" + anchor + "></a>";
    str += "<form name='" + frm + "'>";
    str += "<div style='width:650px;border:4px ridge #fef4f0;background-color:#ffffff;font-family:tahoma;font-size:10pt;'>";
    str += "<div style='height:25px;text-align:center;border-bottom:4px ridge #fef4f0;background-color:#c1cdc1;padding-top:3px;'>Trigonometric calculator <b>[" + formname + "]</b></div>";
    str += "<div style='float:left;width:315px;height:" + hi + "px;text-align:center;'><img src='" + img + "' align='middle' />";
    str += "<br /><font size='2'>" + head + "</font><br />";

    if (rad == 1) {
        str += "<input type='radio' name='d9rad' value='rad' onclick='degrad(this.form.name)' checked>Degree";
        str += "<input type='radio' name='d9rad' value='deg' onclick='degrad(this.form.name)'>Radian";
        str += "&nbsp;&nbsp;&nbsp<input type='button' name='incD9clear' value='Clear All' onclick='clearAll(this.form.name,1);' /><br />";
    }

    str += "<input type='button' name='incdec' value='0.00-' onclick='dec(this.form.name);' />";
    str += "<input type='button' name='incdec' value='0.00+' onclick='inc(this.form.name);' /></div>";
    str += "<div style='float:right;width:310px;'>";
    str += "<table border='1px' width='235px' style='font-family:tahoma;font-size:10pt;'>";
    for (i = 0; i <= tot; i++) {
        if (hili[0] == 999)
            str += "<tr><td width='120px'><input type='text' width='" + width + "px' id='f" + i + "'";
        else {
            if (hili[k] == i) {
                str += "<tr><td bgcolor='" + hico + "><input type='text' width='" + width + "px' disabled id='f" + i + "'";
                k++;
            }
            else
                str += "<tr><td><input type='text' width='" + width + "px' id='f" + i + "'";
        }
        str += " name='f" + i + "' size='16' onkeyup='calc(this.form.name,this.name,this.value);' /> ";
        str += name[i] + "</td></tr>";
    }
    str += "</table>";
    str += "</div></div></form><br />";

    return str;
}

function trig(frm, header, img, name, TextWidth, HeaderColor, hili, rad, head) {
    var str;
    var k = 0;
    var tot = name.length - 1;
  //  var nhi = hili.length - 1;
    
    str = "<a name=" + "anchor" + "></a>";
    str += "<form name='" + frm + "'>";
    str += "<table style='width:662px;height:240px;border:1px solid #345678;'>";
    str += "<tr style='background-color:" + HeaderColor + ";color:white;text-align:center;'><td colspan='2' style='height:22px;font-family:arial;font-size:14px;'>" + header + "</td></tr>";
    str += "</td></tr>";
    
    str += "<tr>";
    str += "<td style='width:310px;text-align:center;font-family:arial;font-size:12px;'>";
    str += "<img src='" + img + "' align='middle' />";
    str += "<br /><font size='2'>" + head + "</font><br />";


    if (rad == 1) {
        str += "<input type='radio' name='d9rad' value='rad' onclick='degrad(this.form.name)' checked>Degree";
        str += "<input type='radio' name='d9rad' value='deg' onclick='degrad(this.form.name)'>Radian";
        str += "&nbsp;&nbsp;&nbsp<input type='button' name='incD9clear' value='Clear All' onclick='clearAll(this.form.name,1);' /><br />";
    }

    str += "<input type='button' name='incdec' value='0.00-' onclick='dec(this.form.name);' />";
    str += "<input type='button' name='incdec' value='0.00+' onclick='inc(this.form.name);' /></div>";

    
    
    str += "</td>";
    str += "<td>";

    str += "<table border='0px' width='340px' style='font-family:tahoma;font-size:10pt;'>";
    for (i = 0; i <= tot; i++) {
        if (hili[0] == 999)
            str += "<tr><td width='40px'><input type='text' size='16' width='" + TextWidth + "px' id='f" + i + "'";
        else {
            if (hili[k] == i) {
                str += "<tr><td bgcolor='#e6e6e6'><input type='text' size='16' width='" + TextWidth + "px' disabled id='f" + i + "'";
                k++;
            }
            else
                str += "<tr><td><input type='text' width='" + TextWidth + "px' id='f" + i + "'";
        }
        str += " name='f" + i + "' size='16' onkeyup='calc(this.form.name,this.name,this.value);' /> ";
        str += name[i] + "</td></tr>";
    }
    str += "</table>";

    
    
    str += "</td>";
    str += "</tr>";
    str += "</table>";
    str += "</form>";
    str += "<div style='height:8px;'></div>"
    
    return str;
}

function trigo(frm, header, img, fname, TextWidth, HeaderColor, hili, rad, head, TableStyle, RightTableStyle,LeftTableStyle, HeaderStyle, clr, range) {
    //clr  - if true then add clear button (true = 1 or -1  false = 0)
    //range - if true add range field
    
    var str;
    var k = 0;
    var tot = fname.length - 1;
    //  var nhi = hili.length - 1;

    str = "<a name=" + "'anchor' />";
    str += "<form name='" + frm + "' id='" + frm + "'>";
    str += "<table " + TableStyle + ">";
    str += "<tr " + HeaderStyle + "><td colspan='2'>" + header + "</td></tr>";
    str += "</td></tr>";

    str += "<tr>";
    str += "<td " + LeftTableStyle + ">";
    str += "<img src='" + img + "' align='right' style='padding-left:20px;' />";
    str += "<br /><font size='2'>" + head + "</font><br />";


    if (rad == 1) {
        str += "<input type='radio' name='d9rad' value='rad' onclick='degrad(this.form.name)' checked>Degree";
        str += "<input type='radio' name='d9rad' value='deg' onclick='degrad(this.form.name)'>Radian";
    }
    if (clr) {
        str += "&nbsp;&nbsp;&nbsp<input type='button' name='incD9clear' value='Clear All' onclick='clearAllFields(this.form.name,1);' />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
    }

    str += "<input type='button' name='incdec' value='0.00-' onclick='dec(this.form.name);' />";
    str += "<input type='button' name='incdec' value='0.00+' onclick='inc(this.form.name);' /></div>";


    str += "</td>";
    str += "<td>";

    str += "<table " + RightTableStyle + ">";
    for (i = 0; i <= tot; i++) {
        if (hili[0] == 999)
            str += "<tr><td width='40px'><input type='text' size='16' width='" + TextWidth + "px' id='f" + i + "'";
        else {
            if (hili[k] == i) {
                str += "<tr><td bgcolor='#e6e6e6'><input type='text' size='16' width='" + TextWidth + "px' disabled id='f" + i + "'";
                k++;
            }
            else
                str += "<tr><td><input type='text' width='" + TextWidth + "px' id='f" + i + "'";
        }
        str += " name='f" + i + "' size='16' onkeyup='calc(this.form.name,this.name,this.value);'  onfocus='calcLovUpv(this.form.name,this.name,this.value);' /> ";
        str += "&nbsp;&nbsp; " + fname[i] + "</td></tr>";
    }

    if (range) {
        str += "<tr><td><input type='text' style='border:0px dashed red;width:240px;margin-top:20px;color:blue;background-color:#e1e1e1;' value=' Input range: ' readonly id='f" + i + "' name='f" + i + "' size='16'></td></tr>";
    }
    str += "</table>";


    str += "</td>";
    str += "</tr>";
    str += "</table>";
    str += "</form>";
    str += "<div style='height:8px;'></div>"

    return str;
}
function BuildTable(frmName, img, imgHeight, name, header, headerColor, headerHigh, head, rad, clear, blocked, blockedColor, txtPadLeft, txtWidth, txtSize, namePadLeft, anchor, headerFont, headerFontSize, headerFontColor) {
    //UPDATE - 12 August 2010
    //name         - fields names
    //header       - of the table
    //headerColor  - color of the header background
    //headerHigh   - header height
    //head         - input note above reset buttons
    //rad == 1     - degree radian button exists 
    //clear == 1   - clear all button exists
    //blocked      - list of all blocked fields - 999 no block
    //blockedColor - blocked fields background color
    //txtPadLeft   - Text padding-left value
    //txtWidth     - Text input field width
    //txtSize      - Text font size in pixls
    //namePadLeft  - description name padding-left value
    //anchor       - place for jump to name
    //headerFont
    var str = "";
    var k = 0;
    var tot = name.length - 1;
    var blk = blocked.length - 1;
    var i = 0;

    str += "<form name=" + frmName + ">";
    str += "<table style='border:1px solid #789abc;width:100%;height:" + imgHeight + ";'>"
    str += "<tr><td colspan='2' style='text-align:center;background-color:" + headerColor + ";height:" + headerHigh + ";font-family:" + headerFont + ";font-size:" + headerFontSize + ";color:" + headerFontColor + "'>" + header + "</td></tr>";
	str += "<tr>"
	str += "<td style='border:0px solid green;'>"
	str += "<img src='/CalcElectric/Images/delta1.png' style='border:0px solid blue;' alt='' /><br/>"

	str += "<br />";
	str += "<div style='text-align:center;'>";
	str += head;
	if (rad) {
	    str += "<input type='radio' name='d9rad' value='rad' onclick='degrad(this.form.name)' checked>Degree";
	    str += "<input type='radio' name='d9rad' value='deg' onclick='degrad(this.form.name)'>Radian";
	}
	if (clear) {
	    str += "&nbsp;&nbsp;&nbsp<input type='button' name='S2Dclear' value='Clear All' onclick='clearAll(this.form.name,1);' /><br />";
	}
	str += "<input type='button' name='incdec' value='0.00-' onclick='dec(this.form.name);' />";
	str += "<input type='button' name='incdec' value='0.00+' onclick='inc(this.form.name);' />";
	str += "</div>";
	str += "</forn>";



	str += "</td>";
	str += "<td style='width:100%;'>";
	str += "<table>";
	for (i = 0; i < name.length; i++) {
	    if (blocked[0] == 999) {
	        str += "<tr style='width:100%;'>";
	        str += "<td style='text-align:left;padding-left:" + txtPadLeft + ";'><input type='text' id='f" + i.toString() + "' style='width:" + txtWidth + ";border:1px solid #456789;font-size:16px;' ";
	    }
	    else {
	        if (blocked[k] == i) {
	            str += "<td style='text-align:left;padding-left:" + txtPadLeft + ";'><input type='text' disabled id='f" + i.toString() + "' style='width:" + txtWidth + ";border:1px solid #456789;font-size:16px;background-color:" + blockedColor + " ";
	            k++;
	        }
	        else {
	            str += "<td style='text-align:left;padding-left:" + txtPadLeft + ";'><input type='text' id='f" + i.toString() + "' style='width:" + txtWidth + ";border:1px solid #456789;font-size:16px;' "; 
	        }
	    }
	    str += "onkeyup='calcS2D(this.id, this.value);'";
	    str += " /></td>";
	    str += "<td style='text-align:left;padding-left:" + namePadLeft + ";'>" + name[i] + "</td>";

	    str += "</tr>";
	}
	str += "</table>"
	str += "</td>"
	str += "</tr>"
	str += "</table>"

	return str;
}

// Rounding numbers -------------------------------------------------------
function round(inVal, plc) {
    inVal = parseFloat(inVal);
    if (isNaN(inVal)) return "";
    if (inVal == 0) return "0";
    if (inVal=="") return "";
    var value = 0;

    //added in order to eliminate very small values
    if (Math.abs(inVal) < set0) return 0;
    
    if (inVal == 0) {
        return inVal;
    }

    var inE = inVal.toString().indexOf("e");

    if (inE == -1) {
        value = Math.round(inVal * Math.pow(10, plc)) / Math.pow(10, plc);
    }
    else {
        //in case of sientific notation
        var newVal = inVal.toString().substring(0, inE);
        var Epart = inVal.toString().substring(inE, inVal.toString().length);
        var calVal = parseFloat(newVal);
        value = Math.round(calVal * Math.pow(10, plc)) / Math.pow(10, plc);
        Epart = value + Epart;
        return Epart;
    }
    
	var i;
	
	if (value != 0)
		return value;
	else
	{
		i = plc + 1;
		while (value==0){
			value = Math.round(inVal * Math.pow(10, i))/Math.pow(10, i);
			i++;
			}
			
			i = i + plc - 1;
		return Math.round(inVal * Math.pow(10, i))/Math.pow(10, i);
	}				
}

function round0(inVal, plc, consider0) {
    if (isNaN(inVal)) return "";

    if (Math.abs(inVal) < consider0)
        return 0;
        
    if (inVal == 0)
        return inVal;

    var value = Math.round(inVal * Math.pow(10, plc)) / Math.pow(10, plc);
    var i;

    if (value != 0)
        return value;
    else {
        i = plc + 1;
        while (value == 0) {
            value = Math.round(inVal * Math.pow(10, i)) / Math.pow(10, i);
            i++;
        }

        i = i + ppl - 1;
        return Math.round(inVal * Math.pow(10, i)) / Math.pow(10, i);
    }
}
// End rounding numbers ---------------------------------------------------


function inc(FormName)
{
	if (ppl < 12)
	{ 
		ppl++;
	    switch (FormName){
	        case "OL":
	            printvCC(name, fv, cc);
	            break;
	        case "WB":
	            printvCC(name, fv, cc);
	            break;
	        default:    
		        printv(name,fv,inIndex);
		}    
	}
}


function dec(FormName)
{
	if (ppl > 0)
	{ 
		ppl--;
		switch (FormName) {
		    case "OL":
		        printvCC(name, fv, cc);
		        break;
		    case "WB":
		        printvCC(name, fv, cc);
		        break;
		    default:
		        printv(name, fv, inIndex);
		}
    }
}


function clearAllFields(FormName, notinuse) {
    var i = 0;
    var tot;
    
    switch (FormName) {
        case "WB":
            tot = 6;
            cc = 0;
            break;
        case "OL":
            tot = 4;
            cc = 0;
            break;

    }
    
    for (i = 0; i < tot; i++) {
        element = document.getElementById('f' + i);
        element.value = "";
        element.style.backgroundColor = "#ffffff";
    }
    element = document.getElementById('f0');
    element.focus();
}


//Print calculated results to the input box.
function printvf(name, frm, fv, infield) {
    //includes form name
    var element;
    var sum = name.length - 1;

    if (ppl == null) ppl = 2;

    for (i = 0; i <= sum; i++) {
        element = document.forms[frm].item('f' + i.toString());

        if (i != infield) {
            if ((fv[i] == 0) || (!isFinite(fv[i])))
                element.value = "";
            else {
                element.value = round(fv[i], ppl);
                element.style.backgroundColor = "#ffffff";
            }
        }
        else {
            if (fv[infield] == 0)
                element.style.backgroundColor = "#ffffff";
            else
                element.style.backgroundColor = "#c7ee6a";
        }
    }
}

function printv(name,fv,infield)
{
		var element;
		var sum = name.length - 1;
		
		if (ppl == null) ppl = 2;
		
     	for (i=0; i<=sum; i++)
     	{
			element = document.getElementById('f' + i);
			
			if (i != infield)
			{
				if ((fv[i]==0) || (!isFinite(fv[i])))
					element.value = "";
				else
				{
					element.value = round(fv[i],ppl);
					element.style.backgroundColor="#ffffff";
				}
			}	
			else
				{
				if (fv[infield]==0)
					element.style.backgroundColor="#ffffff";
				else	
					element.style.backgroundColor="#c7ee6a";
				}
		}			
}


//Print calculated results to the input box.
function printvCC(name, fv, incc) {
    var element;
    var sum = name.length - 1;

    if (ppl == null) ppl = 2;

    for (i = 0; i <= sum; i++) {
        element = document.getElementById('f' + i);

        if ((Math.pow(2, i) & incc) != Math.pow(2, i)) {
            if ((fv[i] == 0) || (!isFinite(fv[i]))) {
                element.value = "";
                element.style.backgroundColor = "#ffffff";
            }
            else {
                element.value = round(fv[i], ppl);
                element.style.backgroundColor = "#ffffff";
            }
        }
        else {
            if (fv[i] == 0)
                element.style.backgroundColor = "#ffffff";
            else
                element.style.backgroundColor = "#c7ee6a";
        }
    }
}


function ccCalc(inx, max, inVal, ccin) {
    var test = ccin;

    if (inVal == 0) {
        test = wCCnoFocus(ccin, inx);
        return (test);
    }
    
    inx = inx.replace("f", "");
    (test |= Math.pow(2, inx));

    if (wCCinputs(test) <= max)
        return (test);
    else
        return (ccin);
}

function wCCunMarkField(inCC, unMarkField) {
    //return total CC W/O unMarkField
    //unMarkField - starting from 0
    var fields = wCCcountInputs(inCC, inCC);
    var CCtot = 0;
    var k = 0;

    while (CCtot < inCC) {
        CCtot += Math.pow(2, k);
        k++;
    }
    CCtot -= Math.pow(2, unMarkField);

    CCtot = CCtot & inCC;
    return (CCtot);
}

function wCCcalc(inCC, inputIndex, inputValue) {
    //inputIndex - starting from 0 - input field number
    //return new CC if field is not marked
    //if field is already marked then return CC unchanged
   
   //if input is 0 then remove the appropriate CC
    if (inputValue == 0)
        return wCCunMarkField(inCC, inputIndex);

    var op = Math.pow(2, inputIndex);

    return (inCC | op);
}

function wCCcalc2(inCC, inputIndex) {
    //inputIndex - starting from 0 - input field number
    //return new CC if field is not marked
    //if field is already marked then return CC unchanged
    var op = Math.pow(2, inputIndex);

    return (inCC | op);
}

function wCCcalcMaxInputs(inCC, inputIndex, inputValue, maxFildes) {
    //maxFields -> starting from 1 for 1 field
    //calculate new CC including input field
    if (inputValue == "") return wCCnoFocus(inCC, inputIndex);
    
    var tmpCC = inCC | Math.pow(2, inputIndex);

    if (wCCinputs(tmpCC) <= maxFildes) 
        return tmpCC;
    else
        return inCC;
}

function wCCcountInputs(CCtotal, CCtest) {
    //count number of inputs (inputs as CC)
    //return 0 if no input found
    var op = CCtotal & CCtest;
    var k = 0;
    var i = 0;

    while (Math.pow(2, i) <= CCtotal) {
        var CC1 = op & Math.pow(2, i);
        var CC2 = Math.pow(2, i);

        if (CC1 == CC2)
            k++;
        i++;
    }
    return k;
}

function wCCisInputAllowed(inCC, inField, maxFields) {
    //inField - field number starting from 0
    //return true if input allowed
    var inputs = wCCcountInputs(inCC, inCC);

    if (inputs < maxFields)
        return true;
    else
        if ((inCC & Math.pow(2, inField)) == Math.pow(2, inField))
        return true;
    else
        return false;
}

function wCCinputs(CCin) {
    //Returns number of inputs = 1 if one input found
    var count = 0;
    var k = 0;
    var test = 0;

    if (CCin == 0)
        return (0);
    else {
        do {
            val = Math.pow(2, k);
            if ((CCin & val) == val) count++;
            k++;
            test += val;
        } while (test < CCin);
    }
    return (count);
}

function wCCnoFocus(cc, focusfield) {
   if ((cc & Math.pow(2,focusfield)) == (Math.pow(2,focusfield)))
        return (cc-Math.pow(2,focusfield));
   else
        return (cc);
}

function wBlockCC(block, inputCC, maxFields) {
    var i = 0;
    var k = 0;
    var tot = 0;
    var tmp = block & inputCC;

    for (i = 1; i <= block; k++, i = Math.pow(2, k)) {
        if ((i & tmp) == i) {
            maxFields--;
        }
    }
    if (maxFields >= 0)
        return true; //OK
    else
        return false; //blocked
}

function wCompWOindx(cc, indx) {
    //gives total cc of all fields except cc of certain field
    var tmp = 0;
    var k = 0;
    var i = 0;

    while (tmp < cc) {
        tmp = Math.pow(2, k);
        k++;
    }

    for (i = 0; i < k - 1; i++)
        tmp += Math.pow(2, i);

    return tmp - Math.pow(2, indx);
}

function wCompWOcc(cc, ccNoField) {
    //gives total cc of all fields except cc of certain field
    var tmp = 0;
    var k = 0;
    var i = 0;

    while (tmp < cc) {
        tmp = Math.pow(2, k);
        k++;
    }

    for (i = 0; i < k - 1; i++)
        tmp += Math.pow(2, i);

    return tmp - ccNoField;
}

function wCompWOinxinx(fields, indx) {
    //calculate cc of total fields (from 0) number
    //return total cc of all fields except cc indx field
    var tmpCC = 0;
    var k = 0;
    var i = 0;

    for (i = 0; i <= fields; i++)
        tmpCC += Math.pow(2, i);

    return tmpCC - Math.pow(2, indx);
}

function wCompWOinxcc(fields, ccNoField) {
    //fields - total fields to calculate
    //gives total cc of all inx fields except cc of certain field
    var tmpCC = 0;
    var k = 0;
    var i = 0;

    for (i = 0; i <= fields; i++)
        tmpCC += Math.pow(2, i);

    return tmpCC - ccNoField;
}

function wCCtotCalc(indx) {
    //indx starting from 0
    var tmpCC = 0;
    var i = 0;

    for (i = 0; i <= indx; i++)
        tmpCC += Math.pow(2, i);

    return tmpCC;

}

function wCCpartial(ccAllowed, ccNotAllowed, ccPresent, ccRequest) {
    //
    //ccAllowed    - cc of all allowed input fields
    //ccNotAllowed - all cc that are prohibited
    //ccPresent    - cc calculated before request
    //ccRequest    - cc of the requested input field
    //return true  - if input is OK  
    //       false - input prohibited
    
    if (ccAllowed == ccPresent) {
        if ((ccNotAllowed & ccRequest) == ccRequest) {
                return false;
        }
    }
    return true;
}

function wCCranges(inputedCC, rangeCC, maxTestFields, testCC) {//  NOT COMPLETED
    //inputedCC     - the range of inputCC that was inputed
    //rangeCC       - cc of other fields that number of inputs are limited
    //maxTestFields - max number of fields allowed in testCC range (starting from 1)
    //testCC        - cc of input field to be examined (ccLine)
    //return  true  - if input is OK  
    //        false - input prohibited
    var i = 0;
    var tmpCC = 1;
    var totCC = 0;

    if ((inputedCC & testCC) == inputedCC) {

        do {
            if ((rangeCC & Math.pow(2, i)) == Math.pow(2, i))
                maxTestFields--;

            i++;
            tmpCC = Math.pow(2, i);

        } while (tmpCC < inputedCC + rangeCC);
    }

    if (maxTestFields < 0)
        return false
    else
        return true

}

function wGroupAllowed(group1, group2, cc, inCC) {
    //dont allow mixing inputs between group1 and group2
    //return true if input is in the same group
    
    //arrange cc to include information from groups only
    cc = ((group1 | group2) & cc);
    if ((cc == 0) || (((group1 & inCC) != inCC) && ((group2 & inCC) != inCC))) return true;

    if ((group1 & cc) == cc) {
        //input was made in first group
        if ((group1 & inCC) == inCC) return true; else return false;
    }
    else if ((group2 & cc) == cc) {
        //input was made in first group 
        if ((group2 & inCC) == inCC) return true; else return false;
    }
    else
        return true;
}

function wGroupsInput(grp1, grp2, TotalCC, inCC) {
    //dont allow mixing inputs between group1 and group2
    //return true if input is in the same group
    //preffered  -> using   [wGroupAllowed]
    var group;

    if ((grp1 & TotalCC) > 0) {
        if ((grp2 & inCC) > 0)
            return false;
        else
            return true;
    }

    if ((grp2 & TotalCC) > 0) {
        if ((grp1 & inCC) > 0)
            return false;
        else
            return true;
    }
}