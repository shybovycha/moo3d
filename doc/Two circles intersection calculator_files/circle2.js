﻿var cc3C = 0;
var pplC = 3;
var m1, m2, m3, i;
var a1, b1, r1, a2, b2, r2, aa1, bb1, cc1, aa2, bb2, cc2;
var fv = new Array();
var ccOK;

function keyPress(ev, id) {
    var tmp;
    var calcFlage = true;
    var num = parseInt(id.replace("a", ""));

    var keycod = ev.keyCode;
    if (((ev.keyCode >= 48) && (ev.keyCode <= 57)) || (ev.keyCode == 46) || (ev.keyCode == 45)) {
        //OK to continue
    }
    else {
        ev.returnValue = false;
        ccOK = false;
        return;
    }

    tmp = cc3C | Math.pow(2, num)

    if (wGroupsInput(7, 448, cc3C, tmp) == false) calcFlage = false;
    if (wGroupsInput(56, 3584, cc3C, tmp) == false) calcFlage = false;

    //input - (minus) on radius is not allowed
    if ((id == "a2") && (ev.keyCode == 45)) calcFlage = false;   
    if ((id == "a5") && (ev.keyCode == 45)) calcFlage = false;  
        
    if (calcFlage == false) {
        ccOK = false;
        ev.returnValue = false;
    }
    else
        ccOK = true;
}

function calc3C(frm, id, value) {
    var OK = false;
    var dis;
    var num = parseInt(id.replace("a", ""));
    document.getElementById('err').innerHTML = "";

    if (ccOK == false) return;     
    
    for (i = 0; i < 5; i++) {
        document.getElementById("f" + i).value = "";
    }

    if (value == "")
        cc3C = cc3C & wCompWOindx(cc3C, num);
    else {
        cc3C = wCCcalc2(cc3C, num);
    }

    for (i = 0; i < 21; i++) {
        fv[i] = 0;
    }
    highLight(cc3C);

    if (ccOK) {
        if ((cc3C & 1) == 1) a1 = parseFloat(circle2.a0.value); else a1 = 0;
        if ((cc3C & 2) == 2) b1 = parseFloat(circle2.a1.value); else b1 = 0;
        if ((cc3C & 4) == 4) r1 = parseFloat(circle2.a2.value); else r1 = 0;
        if ((cc3C & 8) == 8) a2 = parseFloat(circle2.a3.value); else a2 = 0;
        if ((cc3C & 16) == 16) b2 = parseFloat(circle2.a4.value); else b2 = 0;
        if ((cc3C & 32) == 32) r2 = parseFloat(circle2.a5.value); else r2 = 0;

        if ((cc3C & 64) == 64) aa1 = parseFloat(circle2.a6.value); else aa1 = 0;
        if ((cc3C & 128) == 128) bb1 = parseFloat(circle2.a7.value); else bb1 = 0;
        if ((cc3C & 256) == 256) cc1 = parseFloat(circle2.a8.value); else cc1 = 0;
        if ((cc3C & 512) == 512) aa2 = parseFloat(circle2.a9.value); else aa2 = 0;
        if ((cc3C & 1024) == 1024) bb2 = parseFloat(circle2.a10.value); else bb2 = 0;
        if ((cc3C & 2048) == 2048) cc2 = parseFloat(circle2.a11.value); else cc2 = 0;

        if ((cc3C & 7) == 7) {
            circle2.a6.value = round(-2 * a1, pplC);
            circle2.a7.value = round(-2 * b1, pplC);
            circle2.a8.value = round(a1 * a1 + b1 * b1 - r1 * r1, pplC);
        }

        if ((cc3C & 448) == 448) {
            a1 = -aa1 / 2;
            b1 = -bb1 / 2;
            circle2.a0.value = round(-aa1 / 2, pplC);
            circle2.a1.value = round(-bb1 / 2, pplC);
            
            dis = (aa1 * aa1 + bb1 * bb1) / 4 - cc1;
            if (dis < 0) {
                document.getElementById('err').style.color = "red";
                document.getElementById('err').innerHTML = "Radius can not be negative ...";
                return;
            }
            else {
                r1 = Math.sqrt(dis);
                circle2.a2.value = round(r1, pplC);
            }
        }

        if ((cc3C & 56) == 56) {
            circle2.a9.value = round(-2 * a2, pplC);
            circle2.a10.value = round(-2 * b2, pplC);
            circle2.a11.value = round(a2 * a2 + b2 * b2 - r2 * r2, pplC);
        }

        if ((cc3C & 3584) == 3584) {
            a2 = -aa2 / 2;
            b2 = -bb2 / 2;
            circle2.a3.value = round(-aa2 / 2, pplC);
            circle2.a4.value = round(-bb2 / 2, pplC);
            
            dis = (aa2 * aa2 + bb2 * bb2) / 4 - cc2;
            if (dis < 0) {
                document.getElementById('err').style.color = "red";
                document.getElementById('err').innerHTML = "Radius can not be negative ...";
                return;
            }
            else {
                r2 = Math.sqrt(dis);
                circle2.a5.value = round(r2, pplC);
            }
        }

        if (wCCinputs(cc3C) == 6) {
            OK = true;
            var D = Math.sqrt((a2 - a1) * (a2 - a1) + (b2 - b1) * (b2 - b1));

            if (((r1 + r2) >= D) && (D >= Math.abs(r1 - r2))) {
                //two circles intersects
                var eq1;
                var e1;
                var e2;
                var p;
                p = Math.sqrt((D + r1 + r2) * (D + r1 - r2) * (D - r1 + r2) * (-D + r1 + r2)) / 4;
                e1 = (a1 + a2) / 2 + (a2 - a1) * (r1 * r1 - r2 * r2) / (2 * D * D);
                e2 = (b1 + b2) / 2 + (b2 - b1) * (r1 * r1 - r2 * r2) / (2 * D * D);
                var x1 = round(e1 + 2 * (b1 - b2) * p / (D * D), pplC);
                var y1 = round(e2 - 2 * (a1 - a2) * p / (D * D), pplC);
                var x2 = round(e1 - 2 * (b1 - b2) * p / (D * D), pplC);
                var y2 = round(e2 + 2 * (a1 - a2) * p / (D * D), pplC);
                //Intersection coordinates -----------------------------------------------
                fv[0] = e1 + 2 * (b1 - b2) * p / (D * D);
                fv[1] = e2 - 2 * (a1 - a2) * p / (D * D);
                fv[2] = e1 - 2 * (b1 - b2) * p / (D * D);
                fv[3] = e2 + 2 * (a1 - a2) * p / (D * D);

                //Intersection line equation ---------------------------------------------
                var eq0 = "y = " + (a1 - a2) / (b2 - b1) + "x + " + ((r1 * r1 - r2 * r2) + (a2 * a2 - a1 * a1) + (b2 * b2 - b1 * b1)) / (2 * (b2 - b1));
                fv[4] = (a1 - a2) / (b2 - b1);
                fv[5] = ((r1 * r1 - r2 * r2) + (a2 * a2 - a1 * a1) + (b2 * b2 - b1 * b1)) / (2 * (b2 - b1));
                fv[6] = Math.sqrt((a2 - a1) * (a2 - a1) + (b2 - b1) * (b2 - b1));

                fv[7] = (b2 - b1) / (a2 - a1);
                fv[8] = a1 * (b1 - b2) / (a2 - a1) + b1;

                printRes();
                if (((r1 + r2) == D) || (D == Math.abs(r1 - r2))) {
                    document.getElementById('err').style.color = "blue";
                    document.getElementById('err').innerHTML = "Two circles are tangent to each other.";
                }
            }
            else {
                document.getElementById('err').style.color = "red";
                document.getElementById('err').innerHTML = "ERROR: Both circles do not Intersects.";
            }
        }
    }
}

function printRes(){
    var i;
    var arr = new Array();
    arr[0] = "(" + round(fv[0], pplC) + " , " + round(fv[1], pplC) + ")";
    arr[1] = "(" + round(fv[2], pplC) + " , " + round(fv[3], pplC) + ")";
    arr[2] = formatLineEquation(fv[4], fv[5], 1, fv[0], pplC);
    arr[3] = Math.abs(round(fv[6], pplC));
    arr[4] = formatLineEquation(fv[7], fv[8], 1, fv[2], pplC);
    
    for (i=0; i<arr.length; i++){
        document.getElementById("f" + i).value = arr[i];
    }
}

function inc(val) {
    var tmp = pplC + val;

    if ((tmp > 0) || (tmp < 13)) {
        pplC = tmp
        printRes();
    }
}

function clearThis(val) {

    if ((val & 1) == 1) {
        //clear circle 1
        for (i = 0; i < 3; i++) {
            document.getElementById("a" + i).value = "";
        }
        for (i = 6; i < 9; i++) {
            document.getElementById("a" + i).value = "";
        }
        cc3C = cc3C & 3640;
    }

    if ((val & 2) == 2) {
        //clear circle 1
        for (i = 3; i < 6; i++) {
            document.getElementById("a" + i).value = "";
        }
        for (i = 9; i < 12; i++) {
            document.getElementById("a" + i).value = "";
        }
        cc3C = cc3C & 455;
    }
    
    for (i = 0; i < 5; i++) {
        document.getElementById("f" + i).value = "";
    }

    highLight(cc3C);
    document.getElementById('err').value = "";
    circle2.a0.focus();
}

function highLight(cc) {
    for (i = 0; i < 12; i++) {
        if ((cc & Math.pow(2, i)) == Math.pow(2, i))
            document.getElementById("a" + i).style.backgroundColor = "#d3ecc4";
        else
            document.getElementById("a" + i).style.backgroundColor = "white";
    }
}

function getFocus(FocusField, ev) {
    //updates focus field index (number)
    CalcLovUpv(FocusField, 999);
}

function CalcLovUpv(FocusField, inCC) {
    var statusText = "";
    lov = 0;
    upv = 1.0E+300;
    var inpCC = true;
    var testCC = 0;

    var element = document.getElementById('err');
    element.value = "";

    //--------------------------------------------------------------
    if (FocusField == 2) {
        if ((aa1 != undefined) && (bb1 != undefined)) {
            upv = (aa1 * aa1 + bb1 * bb1) / 4;
            if (upv != 0)
                element.value = "Input limit:  " + " " + " " + RangeModeppl("between", pplC);
        }
        return;
    }

    if (FocusField == 5) {
        if ((aa2 != undefined) && (bb2 != undefined)) {
            upv = (aa2 * aa2 + bb2 * bb2) / 4;
            if (upv != 0)
                element.value = "Input limit:  " + " " + " " + RangeModeppl("between", pplC);
        }
        return;
    }
}

function runExample(num) {
    clearThis(3);
    
    switch (num) {
        case 1:
            circle2.a0.value = -4;
            circle2.a1.value = 4;
            circle2.a2.value = 3;
            circle2.a3.value = 0;
            circle2.a4.value = 9;
            circle2.a5.value = 5;
            cc3C = 63;
            break;
        case 2:
            circle2.a0.value = 4;
            circle2.a1.value = -3;
            circle2.a2.value = 5;
            circle2.a3.value = -4;
            circle2.a4.value = -3;
            circle2.a5.value = 3;
            cc3C = 63;
            break;
        case 3:
            circle2.a6.value = 2;
            circle2.a7.value = 0;
            circle2.a8.value = -8;
            circle2.a9.value = -4;
            circle2.a10.value = 10;
            circle2.a11.value = 13;
            cc3C = 4032;
            break;
    }

    ccOK = true;
    calc3C("", "", "0");
}