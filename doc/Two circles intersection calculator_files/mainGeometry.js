﻿
var str = "";

function totalGeometryMenuL() {
    str = "";

    str += "<table style=" + '"' + "border:0px solid #eeeeee;" + '"' + ">";
    str += "<tr>";
    str += "<td style=" + '"' + "vertical-align:top;" + '"' + ">";

    str += "<table id=" + '"' + "lmenu" + '"' + "><tr><td style=" + '"' + "vertical-align:top;text-align:left;" + '"' + ">";
    str += "<div class=" + '"' + "leftH" + '"' + " style=" + '"' + "background-color:#849480;width:183px;padding:3px;" + '"' + ">Analytical 2D Geometry</div>";
    str += "<a href=" + '"' + "/MathCalc/Line/Line_.htm" + '"' + " class=" + '"' + "atag" + '"' + " target=" + '"' + "_top" + '"' + ">Line 2D geometry</a>";
    //str += "<a href=" + '"' + "/TrigoCalc/Line2D/lines2D.htm" + '"' + " class=" + '"' + "atag" + '"' + " target=" + '"' + "_top" + '"' + ">Line 2D geometry</a>";


    str += "<div class=" + '"' + "menuDiv" + '"' + "></div>";
    str += "<a href=" + '"' + "/TrigoCalc/Spher/Circle3D_.htm" + '"' + " class=" + '"' + "atag" + '"' + " target=" + '"' + "_top" + '"' + ">Circle defined by 3 points</a>";
    str += "<div class=" + '"' + "menuDiv" + '"' + "></div>";
    str += "<a href=" + '"' + "/MathCalc/Line/LinePointDistance.htm" + '"' + " class=" + '"' + "atag" + '"' + " target=" + '"' + "_top" + '"' + ">Distance of a point to line</a>";
    str += "<div class=" + '"' + "menuDiv" + '"' + "></div>";
    str += "<a href=" + '"' + "/TrigoCalc/Circles2/CirclePoint/CirclePointDistance.htm" + '"' + " class=" + '"' + "atag" + '"' + " target=" + '"' + "_top" + '"' + ">Distance of a point to circle</a>";
    str += "<div class=" + '"' + "menuDiv" + '"' + "></div>";
    str += "<a href=" + '"' + "/TrigoCalc/Circles2/CircleCircleIntersection.htm" + '"' + " class=" + '"' + "atag" + '"' + " target=" + '"' + "_top" + '"' + ">Intersection of two circles</a>";
    str += "<div class=" + '"' + "menuDiv" + '"' + "></div>";
    str += "<a href=" + '"' + "/MathCalc/Line/Line_.htm" + '"' + " class=" + '"' + "atag" + '"' + " target=" + '"' + "_top" + '"' + ">Intersection of two lines</a>";
    str += "<div class=" + '"' + "menuDiv" + '"' + "></div>";
    str += "<a href=" + '"' + "/TrigoCalc/Circles2/circlrLine_.htm" + '"' + " class=" + '"' + "atag" + '"' + " target=" + '"' + "_top" + '"' + ">Intersection of circle and line</a>";
    str += "<div class=" + '"' + "menuDiv" + '"' + "></div>";
    str += "<a href=" + '"' + "/TrigoCalc/Circles2/Ellipse/EllipseLine.htm" + '"' + " class=" + '"' + "atag" + '"' + " target=" + '"' + "_top" + '"' + ">Intersection ellipse and line</a>";
    str += "<div class=" + '"' + "menuDiv" + '"' + "></div>";
    str += "<a href=" + '"' + "/TrigoCalc/PointInOut_.htm" + '"' + " class=" + '"' + "atag" + '"' + " target=" + '"' + "_top" + '"' + ">Point inside triangle or circle</a>";
    str += "<div class=" + '"' + "menuDiv" + '"' + "></div>";
    str += "<a href=" + '"' + "/TrigoCalc/Circles2/Circles2Tangent_.htm" + '"' + " class=" + '"' + "atag" + '"' + " target=" + '"' + "_top" + '"' + ">Tangent lines to 2 circles</a>";
    str += "<div class=" + '"' + "menuDiv" + '"' + "></div>";
    str += "<a href=" + '"' + "/TrigoCalc/Triangle3lines_.htm" + '"' + " class=" + '"' + "atag" + '"' + " target=" + '"' + "_top" + '"' + ">Triangle defined by 3 lines</a>";
    str += "<div class=" + '"' + "menuDiv" + '"' + "></div>";
    str += "<a href=" + '"' + "/TrigoCalc/Triangle3D_.htm" + '"' + " class=" + '"' + "atag" + '"' + " target=" + '"' + "_top" + '"' + ">Triangle defined by 3 points</a>";
    str += "<div class=" + '"' + "menuDiv" + '"' + "></div>";

    str += "<div class=" + '"' + "leftH" + '"' + " style=" + '"' + "background-color:#849480;width:183px;padding:3px;" + '"' + ">Analytical 3D Geometry</div>";
    str += "<a href=" + '"' + "/TrigoCalc/Line3D/Line3D_.htm" + '"' + " class=" + '"' + "atag" + '"' + " target=" + '"' + "_top" + '"' + ">Line 3D geometry</a>";
    str += "<div class=" + '"' + "menuDiv" + '"' + "></div>";
    str += "<a href=" + '"' + "/TrigoCalc/Line3D/LineColinear.htm" + '"' + " class=" + '"' + "atag" + '"' + " target=" + '"' + "_top" + '"' + ">Collinear points</a>";
    str += "<div class=" + '"' + "menuDiv" + '"' + "></div>";
    str += "<a href=" + '"' + "/TrigoCalc/Plan3D/PointsCoplanar.htm" + '"' + " class=" + '"' + "atag" + '"' + " target=" + '"' + "_top" + '"' + ">Coplanar points</a>";
    str += "<div class=" + '"' + "menuDiv" + '"' + "></div>";
    str += "<a href=" + '"' + "/TrigoCalc/Line3D/Distance2Lines3D_.htm" + '"' + " class=" + '"' + "atag" + '"' + " target=" + '"' + "_top" + '"' + ">Distance between 2 lines</a>";
    str += "<div class=" + '"' + "menuDiv" + '"' + "></div>";
    str += "<a href=" + '"' + "/TrigoCalc/Plan3D/Plane3D_.htm" + '"' + " class=" + '"' + "atag" + '"' + " target=" + '"' + "_top" + '"' + ">Plane equation</a>";
    str += "<div class=" + '"' + "menuDiv" + '"' + "></div>";
    str += "<a href=" + '"' + "/TrigoCalc/Plan3D/Plane3D_.htm" + '"' + " class=" + '"' + "atag" + '"' + " target=" + '"' + "_top" + '"' + ">Plane defined by 3 points</a>";
    str += "<div class=" + '"' + "menuDiv" + '"' + "></div>";
    str += "<a href=" + '"' + "/TrigoCalc/Plan3D/PlaneLineIntersection_.htm" + '"' + " class=" + '"' + "atag" + '"' + " target=" + '"' + "_top" + '"' + ">Plane and line intersection</a>";
    str += "<div class=" + '"' + "menuDiv" + '"' + "></div>";
    str += "<a href=" + '"' + "/TrigoCalc/Plan3D/3PlanesIntersection_.htm" + '"' + " class=" + '"' + "atag" + '"' + " target=" + '"' + "_top" + '"' + ">Planes intersection</a>";
    str += "<div class=" + '"' + "menuDiv" + '"' + "></div>";
    str += "<a href=" + '"' + "/TrigoCalc/Spher/Spher3D_.htm" + '"' + " class=" + '"' + "atag" + '"' + " target=" + '"' + "_top" + '"' + ">Sphere defined by 4 points</a>";
    str += "<div class=" + '"' + "menuDiv" + '"' + "></div>";
    str += "<a href=" + '"' + "/TrigoCalc/Spher/SpherLineIntersection_.htm" + '"' + " class=" + '"' + "atag" + '"' + " target=" + '"' + "_top" + '"' + ">Sphere and line intersection</a>";
    str += "<div class=" + '"' + "menuDiv" + '"' + "></div>";
    str += "<a href=" + '"' + "/TrigoCalc/Spher/SpherePlaneIntersection_.htm" + '"' + " class=" + '"' + "atag" + '"' + " target=" + '"' + "_top" + '"' + ">Sphere & plane intersection</a>";
    str += "<div class=" + '"' + "menuDiv" + '"' + "></div>";

    str += "<div class=" + '"' + "leftH" + '"' + " style=" + '"' + "background-color:#849480;width:183px;padding:3px;" + '"' + ">Geometry</div>";
    str += "<a href=" + '"' + "/TrigoCalc/Spher/Arc_.htm" + '"' + " class=" + '"' + "atag" + '"' + " target=" + '"' + "_top" + '"' + ">Arc and circle</a>";
    str += "<div class=" + '"' + "menuDiv" + '"' + "></div>";
    str += "<a href=" + '"' + "/TrigoCalc/Circles2/Ellipse/Ellipse.htm" + '"' + " class=" + '"' + "atag" + '"' + " target=" + '"' + "_top" + '"' + ">Ellipse</a>";
    str += "<div class=" + '"' + "menuDiv" + '"' + "></div>";
    str += "<a href=" + '"' + "/TrigoCalc/Parallelogram/Rhombus_.htm" + '"' + " class=" + '"' + "atag" + '"' + " target=" + '"' + "_top" + '"' + ">Parallelogram</a>";
    str += "<div class=" + '"' + "menuDiv" + '"' + "></div>";
    str += "<a href=" + '"' + "/TrigoCalc/Polygon_.htm" + '"' + " class=" + '"' + "atag" + '"' + " target=" + '"' + "_top" + '"' + ">Polygons</a>";
    str += "<div class=" + '"' + "menuDiv" + '"' + "></div>";
    str += "<a href=" + '"' + "/TrigoCalc/Cone_.htm" + '"' + " class=" + '"' + "atag" + '"' + " target=" + '"' + "_top" + '"' + ">Pyramid and cone</a>";
    str += "<div class=" + '"' + "menuDiv" + '"' + "></div>";
    str += "<a href=" + '"' + "/TrigoCalc/Spher/Calculator/SphereCalc.htm" + '"' + " class=" + '"' + "atag" + '"' + " target=" + '"' + "_top" + '"' + ">Spherical Segment</a>";
    str += "<div class=" + '"' + "menuDiv" + '"' + "></div>";
    //str += "<div style='height:40px;'></div>";
    str += "</td></tr></table>";
    str += "</td></tr>";
    str += "</table>";
    str += "</div>";

    return str;
}

function totalGeometryHead() {
    str = "";
    var lft = (screen.width - 550) / 2;
    
    str += "<img src=" + '"' + "/Images/BackGrounds/headdev03.PNG" + '"' + " alt=" + '"' + "Home page" + '"' + " title=" + '"' + "Home page" + '"' + " style=" + '"' + "border:0px;" + '"' + " onclick=" + '"' + "window.open('/default.aspx','_top')" + '"' + " onmousemove=" + '"' + "this.style.cursor = 'hand';" + '"' + " />";
    str += "<div style=" + '"' + "width:900px;height:26px;border:0px solid #1bbbbb;text-align:left;padding-left:80px; background-image:url(" + "'/Images/BackGrounds/bc7.png');background-repeat:repeat-x;" + '">';
    str += "<div class=" + '"' + "menuUp" + '"' + " style=" + '"' + "background-color:#999999;" + '"' + " onmouseover=" + '"' + "style.cursor='hand'; style.backgroundImage='none'; style.color='white'" + '"' + " onmouseout=" + '"' + "this.style.backgroundColor='#999999'; style.color='black'" + '"' + " onclick=" + '"' + "navigate('/default.aspx')" + '"' + ">Home</div>";
    str += "<div class=" + '"' + "menuUp1" + '"' + ">|</div>";
    str += "<div class=" + '"' + "menuUp" + '"' + " onmouseover=" + '"' + "style.cursor='hand'; style.backgroundImage='none'; style.backgroundColor='#d4f344'" + '"' + " onmouseout=" + '"' + "this.style.backgroundImage='url(/Images/BackGrounds/bc7.png)'" + '"' + " onclick=" + '"' + "navigate('/Equations/Circle/EQ-Circle.aspx')" + '"' + ">Circle Equations</div>";
    str += "<div class=" + '"' + "menuUp1" + '"' + ">|</div>";
    str += "<div class=" + '"' + "menuUp" + '"' + " onmouseover=" + '"' + "style.cursor='hand'; style.backgroundImage='none'; style.backgroundColor='#99d488'" + '"' + " onmouseout=" + '"' + "this.style.backgroundImage='url(/Images/BackGrounds/bc7.png)'" + '"' + " onclick=" + '"' + "navigate('/Equations/Triangle/Triangle.aspx')" + '"' + ">Triangle Equations</div>";
    str += "<div class=" + '"' + "menuUp1" + '"' + ">|</div>";
    str += "<div class=" + '"' + "menuUp" + '"' + " onmouseover=" + '"' + "style.cursor='hand'; style.backgroundImage='none'; style.backgroundColor='#db9cf5'" + '"' + " onmouseout=" + '"' + "this.style.backgroundImage='url(/Images/BackGrounds/bc7.png)'" + '"' + " onclick=" + '"' + "navigate('/Equations/Lines/Line.aspx')" + '"' + ">Line Geometry</div>";
    str += "<div class=" + '"' + "menuUp1" + '"' + ">|</div>";
    str += "<div class=" + '"' + "menuUp" + '"' + " onmouseover=" + '"' + "style.cursor='hand'; style.backgroundImage='none'; style.backgroundColor='#75dea3'" + '"' + " onmouseout=" + '"' + "this.style.backgroundImage='url(/Images/BackGrounds/bc7.png)'" + '"' + " onclick=" + '"' + "navigate('/Equations/Trigonometry/Trigonometry.aspx')" + '"' + ">Trigonometry</div>";
    str += "<img src='/Images/Buttons/email3.PNG' id='reportImg' alt='' style='margin-left:280px;height:16px;padding-top:5px;cursor:pointer;' onclick='window.open(\"/Comments/sendError.aspx\",\"\", \"width=550,height=580,top=200,left=" + lft + "\")' />";
    str += "</div>"

    return str;
}

function totalGeometryFooter() {
    str = "";

    str += "<table align=" + '"' + "center" + '"' + " style=" + '"' + "width:980px;background-color:#bec3b3;border-top:6px solid #9aa48a;font:normal 12px Tahoma;" + '"' + ">";
    str += "<tr style=" + '"' + "vertical-align:middle;" + '"' + ">";
    str += "<td style=" + '"' + "width:220px;text-align:left;padding-left:9px;" + '"' + "><img src=" + '"' + "/Images/BackGrounds/AMBRweb1.png" + '"' + " style=" + '"' + "border:0px;height:16px;" + '"' + " alt=" + '"' + "AmBrSoft development" + '"' + "/></td>";
    str += "<td style=" + '"' + "vertical-align:top;padding-top:6px;text-align:left;" + '"' + "><i>AmBrSoft &nbsp;&nbsp;03-2012</i></td>";
    str += "<td style=" + '"' + "border:0px solid red;" + '"' + "><a href=" + '"' + "/GeneralCalc/Disclaimer.aspx" + '"' + " target=" + '"' + "main" + '"' + "><i>Disclaimer</i></a></td>";
    str += "<td style=" + '"' + "text-align:right;" + '"' + "><a href=" + '"' + "#top" + '"' + "><img src=" + '"' + "/Images/BackGrounds/top.png" + '"' + " alt=" + '"' + "Go to top of the page" + '"' + " style=" + '"' + "border:0px;" + '"' + " /></a></td>";
    str += "<td style=" + '"' + "text-align:right;padding-top:4px;padding-right:8px;" + '"' + ">";
    str += "<a ID=" + '"' + "btnSendToFriend" + '"' + " runat=" + '"' + "server" + '"' + "  onclick=" + '"' + "window.open('/Comments/EmailTo.aspx','name_',' width=450, height=400, left=400, top=200');" + '"' + ">";
    str += "<img src=" + '"' + "/Images/BackGrounds/sendFriend3.PNG" + '"' + " style=" + '"' + "height:12px;" + '"' + " alt=" + '"' + "Send a link to this site to a friend" + '"' + " onmouseover=\"style.cursor=" + "'hand'\"" + " /></a>";
    str += "</td>";
    str += "</tr>";
    str += "</table>";
    str += "<img src=" + '"' + "/TrigoCalc/Images/analytical.PNG" + '"' + " alt=" + '"' + '"' + '"' + " style=" + '"' + "position:fixed;left:0px;top:0px;visibility:visible;border:0px;" + '"' + " />";

    return str;
}

function totalGeometryFooterV1(upDate) {
    //this update anables of individual date update
    str = "";

    str += "<table align=" + '"' + "center" + '"' + " style=" + '"' + "width:980px;height:45px; background-color:#bec3b3;border-top:11px solid #9aa48a;font:normal 12px Tahoma;" + '"' + ">";
    str += "<tr style=" + '"' + "vertical-align:middle;" + '"' + ">";
    str += "<td style=" + '"' + "width:220px;text-align:left;padding-left:9px;" + '"' + "><img src=" + '"' + "/Images/BackGrounds/AMBRweb1.png" + '"' + " style=" + '"' + "border:0px;height:16px;" + '"' + " alt=" + '"' + "AmBrSoft development" + '"' + "/></td>";
    str += "<td style=" + '"' + "text-align:left;" + '"' + "><i>AmBrSoft &nbsp;&nbsp;" + upDate + "</i></td>";
    str += "<td style=" + '"' + "border:0px solid red;" + '"' + "><a href=" + '"' + "/GeneralCalc/Disclaimer.aspx" + '"' + " target=" + '"' + "main" + '"' + "><i>Disclaimer</i></a></td>";
    str += "<td style=" + '"' + "text-align:right;" + '"' + "><a href=" + '"' + "#top" + '"' + "><img src=" + '"' + "/Images/BackGrounds/top.png" + '"' + " alt=" + '"' + "Go to top of the page" + '"' + " style=" + '"' + "border:0px;" + '"' + " /></a></td>";
    str += "<td style=" + '"' + "text-align:right;padding-top:4px;padding-right:8px;" + '"' + ">";
    str += "<a ID=" + '"' + "btnSendToFriend" + '"' + " runat=" + '"' + "server" + '"' + "  onclick=" + '"' + "window.open('/Comments/EmailTo.aspx','name_',' width=450, height=400, left=400, top=200');" + '"' + ">";
    str += "<img src=" + '"' + "/Images/BackGrounds/sendFriend3.PNG" + '"' + " style=" + '"' + "height:12px;" + '"' + " alt=" + '"' + "Send a link to this site to a friend" + '"' + " onmouseover=\"style.cursor=" + "'hand'\"" + " /></a>";
    str += "</td>";
    str += "</tr>";
    str += "</table>";
    str += "<img id='AD' src=" + '"' + "/Equations/equationCenter.PNG" + '"' + " alt=" + '"' + '"' + '"' + " style=" + '"' + "position:fixed;left:0px;top:0px;visibility:visible;border:0px;" + '"' + " />";

    return str;
}
