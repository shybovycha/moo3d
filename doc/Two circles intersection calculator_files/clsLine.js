﻿//**********************************************************************************
//        Updated  12-22-11
// function point(x, y)
// function midPoint(x1, y1, x2, y2)           function pmidPoint(p1, p2)
// function distancePoint2Line(xp, yp, a, b)   function pdistancePoint2Line(p, a, b)
// function distance2Points(x1, y1, x2, y2)
// function intersection(m1, a1, m2, a2)
// function intersection2lines(a1, b1, c1, a2, b2, c2)
// function IntersectionOfMedians(p1, p2, p3)
// function intrOfAltitudes(p1, p2, p3)
// function intrOfBisectors(p1, p2, p3)
// function intrOfAngleBisector(p1, p2, p3)
// function angleToX(m)
// function angleOf2Lines(m1, m2)
// function angleOf2LinesRibs(ang, b, c)
// function angleOf2Points(x1, y1, x2, y2)
// function area3lines(m1, a1, m2, a2, m3, a3)
// function formatLineEquation(a, b, y, x1, ppl)
// function formatLineEqu(x1, y1, x2, y2, ppl)
// function formatLineNormal(x1, y1, x2, y2, ppl)
// function formatLinePointSlope(x, y, m, ppl)
// function setParamLineEquation(a, at)
// 
//    ****** 3D plane and line *****************************************************
// function point3D(x, y, z)
// function midPoint3D(x1, y1, x2, y2, x3, y3)    function pmidPoint3D(p1, p2)
// function pdistance3D(p1, p2)
// function angleBetween2Planes(a1, b1, c1, a2, b2, c2)
// function Distance2Lines3D(x1, y1, z1, a1, b1, c1, x2, y2, z2, a2, b2, c2)
// function formatPlaneEqu(A, B, C, D, ppl)
// function isPointsCollinear(mat)
// function isPlaneCoPlaner(mat)
//----------------------------------------------------------------------------------
//
//For a new point write:    p1 = new  point(3, 6)

function point(x, y) { this.x = x; this.y = y; }


function midPoint(x1, y1, x2, y2) {
    return new point((x1 + x2) / 2, (y1 + y2) / 2);
}


function pmidPoint(p1, p2) {
    return new point((p1.x + p2.x) / 2, (p1.y + p2.y) / 2);
}


function distancePoint2Line(xp, yp, a, b) {
    return Math.abs(a * xp - yp + b) / Math.sqrt(a * a + 1);
}


function pdistancePoint2Line(p, a, b) {
    return Math.abs(a * p.x - p.y + b) / Math.sqrt(a * a + 1);
}

function distance2Points(x1, y1, x2, y2) {
    return Math.sqrt((x2 - x1) * (x2 - x1) + (y2 - y1) * (y2 - y1));
}

function intersection(m1, a1, m2, a2) {
    var p1 = (a2 - a1) / (m1 - m2);
    var p2 = (m1 * a2 - m2 * a1) / (m1 - m2);
    return new point((a2 - a1) / (m1 - m2), (m1 * a2 - m2 * a1) / (m1 - m2));
}

function intersection2lines(a1, b1, c1, a2, b2, c2) {
    //recives line of form:    ax + by + c = 0
    var x = (b1 * c2 - b2 * c1) / (a1 * b2 - a2 * b1);
    var y = (a1 * c2 - a2 * c1) / (a2 * b1 - a1 * b2);

    return new point(x, y);
}

function IntersectionOfMedians(p1, p2, p3) {
    //Tree points
    return new point((p1.x + p2.x + p3.x) / 3, (p1.y + p2.y + p3.y) / 3);
}

function intrOfAltitudes(p1, p2, p3) {
    var a0 = p1.y * (p3.x * p1.x + p2.y * p2.y - p1.x * p2.x - p3.y * p3.y);
    var a1 = (p2.x * p3.x + p1.y * p1.y) * (p2.y - p3.y);
    var a2 = p2.y * (p1.x * p2.x + p3.y * p3.y);
    var a3 = p3.y * (p3.x * p1.x + p2.y * p2.y);
    var c0 = (p1.x * p1.x + p2.y * p3.y) * (p2.x - p3.x);
    var c1 = p1.x * (p2.x * p2.x + p3.y * p1.y);
    var c2 = p1.x * (p3.x * p3.x + p1.y * p2.y);
    var c3 = p3.x * (p2.x * p2.x + p3.y * p1.y) - p2.x * (p3.x * p3.x + p1.y * p2.y);
    var b0 = p1.x * (p2.y - p3.y) - p1.y * (p2.x - p3.x) + p2.x * p3.y - p3.x * p2.y;

    return new point((a0 - a1 + a2 - a3) / b0, (c0 - c1 + c2 + c3) / b0);
}

function intrOfBisectors(p1, p2, p3) {
    var a0 = (p1.x * p1.x + p1.y * p1.y) * (p2.y - p3.y);
    var a1 = p1.y * (p2.x * p2.x + p2.y * p2.y - p3.x * p3.x - p3.y * p3.y);
    var a2 = p2.y * (p3.x * p3.x + p3.y * p3.y);
    var a3 = p3.y * (p2.x * p2.x + p2.y * p2.y);
    var c0 = p1.x * (p2.x * p2.x + p2.y * p2.y - p3.x * p3.x - p3.y * p3.y);
    var c1 = p2.x * (p3.x * p3.x + p3.y * p3.y);
    var c2 = p3.x * (p2.x * p2.x + p2.y * p2.y);
    var c3 = (p1.x * p1.x + p1.y * p1.y) * (p2.x - p3.x);
    var b0 = 2 * (p1.x * (p2.y - p3.y) - p1.y * (p2.x - p3.x) + p2.x * p3.y - p3.x * p2.y);

    return new point((a0 - a1 - a2 + a3) / b0, (c0 + c1 - c2 - c3) / b0);
}

function intrOfAngleBisector(p1, p2, p3) {
    var a = Math.sqrt((p2.x - p1.x) * (p2.x - p1.x) + (p2.y - p1.y) * (p2.y - p1.y));
    var b = Math.sqrt((p3.x - p1.x) * (p3.x - p1.x) + (p3.y - p1.y) * (p3.y - p1.y));
    var c = Math.sqrt((p3.x - p2.x) * (p3.x - p2.x) + (p3.y - p2.y) * (p3.y - p2.y));

    return new point((a * p3.x + b * p2.x + c * p1.x) / (a + b + c), (a * p3.y + b * p2.y + c * p1.y) / (a + b + c));
}

function interceptX(a, b, c) {
    // Input:  line equation ax + by + c = 0
    // y = 0 return x coordinate
    
    if (a == 0)
    //line is parallel and do not intersects x axes
        return "";
    else
        return -c / a;
}

function interceptY(a, b, c) {
    // Input:  line equation ax + by + c = 0
    // x = 0 return x coordinate

    if (b == 0)
    //line is parallel and do not intersects y axes
        return "";
    else
        return -c / b;
}

function interceptPoint(a, b, c) {
    var ret = new point();

    if (a == 0)
    //line is parallel and do not intersects y axes
        ret.x = "";
    else
        ret.x = -c / a;

    if (b == 0)
    //line is parallel and do not intersects y axes
        ret.y= "";
    else
        ret.y= -c / b;

    return ret;
}

function angleToX(m) {
    var alfa = Math.atan(m);

    if (m < 0)
        return Math.PI + alfa;
    else
        return alfa;
}

function angleOf2Lines(m1, m2) {
    if (m1 == m2) return 0;
    
    var tmp = (m1 - m2) / (1 + m1 * m2);

    return Math.atan(Math.abs(tmp));
}

function angleBetween2Lines(a1, b1, a2, b2) {
    //input is part of line equation:   ax + by + .. = 0
    if (b1 / a1 != b2 / a2)
        var tmp = (a1 * b2 - b1 * a2) / (b1 * b2 + a1 * a2);
    else
        return 0;

    return Math.atan(Math.abs(tmp));
}

function angleOf2LinesRibs(ang, b, c) {
    return Math.atan((b * b + c * c - ang * ang) / (2 * b * c));
}

function angleOf2Points(x1, y1, x2, y2) {
    //return angle in radians
    
    if ((x1 == x2) && (y1 != y2))
        return Math.PI / 2;

    var ang = (y2 - y1) / (x2 - x1);

    if (ang < 0)
        return Math.PI + ang;
    else
        return ang;
}

function area3lines(m1, a1, m2, a2, m3, a3) {
    var tmp = m1 * (a3 - a2) + m2 * (a1 - a3) + m3 * (a2 - a1);
    return Math.abs(tmp * tmp / (2 * (m1 - m2) * (m2 - m3) * (m3 - m1)));
}

function areaPf3lines(a1, b1, c1, a2, b2, c2, a3, b3, c3) {
    //lines of the form:    ax + by + c = 0
    var x1 = (b1 * c2 - c1 * b2) / (a1 * b2 - b1 * a2);
    var y1 = (c1 * a2 - a1 * c2) / (a1 * b2 - b1 * a2);

    var x2 = (b1 * c3 - c1 * b3) / (a1 * b3 - b1 * a3);
    var y2 = (c1 * a3 - a1 * c3) / (a1 * b3 - b1 * a3);

    var x3 = (b2 * c3 - c2 * b3) / (a2 * b3 - b2 * a3);
    var y3 = (c2 * a3 - a2 * c3) / (a2 * b3 - b2 * a3);
    
    var tmp = x1 * (y2 - y3) + x2 * (y3 - y1) + x3 * (y1 - y2);
    return Math.abs(tmp / 2);
}


// Format line equations ******************************************
function formatLineEquation(a, b, y, x1, ppl) {
    //format to form:  y = ax + b 
    var str = "";
    if ((isNaN(a) || a == "") && (isNaN(b) || b == ""))
        return "";
    else {
        if (isNaN(a)) a = 0;
        if (isNaN(b)) b = 0;
        if (isNaN(y)) y = 0;
    }

    if (y != 0) {
        if (y == 1)
            str += "y";
        else
            str += round(y, ppl) + "y";

        str += " = ";
    }

    if (a == 0)
        str += "";
    else if (Math.abs(a) == 1) {
        if (a < 0) str += "-";
        str += "x";
    }
    else if ((a == Infinity) || (a == -Infinity)) {
        return "x = " + round(x1, ppl);
    }
    else
        str += round(a, ppl) + "x";

    if (b != 0) {
        if (y == 0) {
            str += " = " + (-round(b, ppl));
            return str;
        }
        if (b < 0)
            str += "-";
        else {
            if (a != 0)
                str += "+";
        }

        str += Math.abs(round(b, ppl));
    }
    return str;
}

function formatLineEqu(x1, y1, x2, y2, ppl) {
    //return line equation form:     ay + bx + c = 0
    //   inputs two points of the line
    var str = "";

    if (x1 == x2) {
        //line parallel to y axis
        return "x = " + round(x1, ppl);
    }
    else if (y1 == y2) {
        //line parallel to x axis
        return "y = " + round(y1, ppl);
    }
        
    var a = round(1 / (y2 - y1), ppl);
    var b = round(-1 / (x2 - x1), ppl);
    var c = round(-y1 / (y2 - y1) + x1 / (x2 - x1), ppl);


    if (a == 0)
        str = "";
    else if (a == 1)
        str = "y";
    else if (a == -1)
        str = "-y";
    else if (a == Infinity)
        return "y = " + b;
    else if (a == -Infinity)
        return "y = -" + Math.abs(b);
    else
        str = a + "y";


    if (b == 1)
        str += " + x";
    else if (b == -1)
        str += " - x";
    else if (b > 0)
        str += " + " + b + "x";
    else if (b < 0)
        str += " - " + Math.abs(b) + "x";
    else if (b == Infinity)
        return "x = " + a;
    else if (b == -Infinity)
        return "x = -" + Math.abs(a);

    if (c == 0)
        str += " = 0";
    else if (c > 0)
        str += " + " + c + " = 0";
    else if (c < 0)
        str += " - " + Math.abs(c) + " = 0";

    return str;
}

function formatLineEquXY(x1, y1, x2, y2, ppl, plus) {
    //return line equation form:     ay + bx + c = 0
    //   inputs two points of the line
    // if plus = true then -x turns to +x
    var str = "";
    var fac;

    if (x1 == x2) {
        //line parallel to y axis
        return "x = " + round(x1, ppl);
    }
    else if (y1 == y2) {
        //line parallel to x axis
        return "y = " + round(y1, ppl);
    }

    var a = round(-1 / (x2 - x1), ppl);
    var b = round(1 / (y2 - y1), ppl);
    var c = round(-y1 / (y2 - y1) + x1 / (x2 - x1), ppl);

    if ((plus) && (a < 0)) {
        a = -a;
        b = -b;
        c = -c;
    }

    if (a == 0)
        str = "";
    else if (a == 1)
        str = "x";
    else if (a == -1)
        str = "-x";
    else if (a == Infinity)
        return "x = " + b;
    else if (a == -Infinity)
        return "x = -" + Math.abs(b);
    else
        str = a + "x";


    if (b == 1)
        str += " + y";
    else if (b == -1)
        str += " - y";
    else if (b > 0)
        str += " + " + b + "y";
    else if (b < 0)
        str += " - " + Math.abs(b) + "y";
    else if (b == Infinity)
        return "y = " + a;
    else if (b == -Infinity)
        return "y = -" + Math.abs(a);

    if (c == 0)
        str += " = 0";
    else if (c > 0)
        str += " + " + c + " = 0";
    else if (c < 0)
        str += " - " + Math.abs(c) + " = 0";

    return str;
}

function formatLine(a, b, c, ppl) {
    //input in the same manner:      ax + by + c = 0
    //return line equation form:     ax + by + c = 0
    //   inputs two points of the line
    var str = " ";
    var fac;

    if ((a == 0) && (b == 0)) return str;
    if ((a == undefined) || (b == undefined) || (c == undefined)) return str;
    if (a == Infinity) b = 0;
    
    if (a < 0) fac = -1; else fac = 1;

    a *= fac;
    a = round(a, ppl);
    if (a == 0) {
        if (b < 0) fac = -1; else fac = 1;
        str = "";
    }
    else if (a == 1)
        str = "x ";
    else if (a == -1)
        str = "-x ";
    else
        str = " " + a + "x ";


    b *= fac;
    b = round(b, ppl);
    if (b == 1) {
        if (a == 0)
            str += "y ";
        else
            str += " + y";
    }
    else if (b == -1) {
        if (a == 0)
            str += "-y ";
        else
            str += "- y ";
    }
    else if (b > 0) {
        if (a != 0)
            str += " + " + b + "y ";
        else
            str += b + "y ";
    }
    else if (b < 0)
        str += " - " + Math.abs(b) + "y ";
    else if (b == Infinity)
        return "y = " + a;
    else if (b == -Infinity)
        return "y = -" + Math.abs(a) + " ";


    c *= fac;
    c = round(c, ppl);
    if (c == 0)
        str += "";
    else if (c > 0)
        str += "+ " + c;
    else if (c < 0)
        str += "- " + Math.abs(c) + " ";

    str += "= 0";
    return str;
}

function formatLineN(y, x, c, ppl, type) {
    //***********************************************************
    //   Prefarable fuction to use        03-22-12
    //***********************************************************
    //return line equation form:     by = ax + c
    //   inputs two points of the line
    // type = 0  ->  by = ax + c
    // type = 1  ->  y = (a/b)x + (c/b)
    //
    var str = "";

    if (x == Infinity) y = 0;
    if ((y == 0) && (x == 0)) return str;
    if ((y == 0) && (c == 0)) return "x = 0";
    if ((x == 0) && (c == 0)) return "y = 0";
    if ((y == 0) && (x == 0) && (c == 0)) return str;
    if ((y == undefined) || (x == undefined) || (c == undefined)) return str;
    if (isNaN(x) || isNaN(y) || isNaN(c)) return str;
    if (x == "") x = 0; if (y == "") y = 0; if (c == "") c = 0; ;

    if ((y != 0) && (type == 1)) {
        x = x / y;
        c = c / y;
        y = y / y; //must be in this order
    }

    if ((y < 0) && (type == 0)) {
        x = -x;
        y = -y;
        c = -c;
    }

    x = round(x, ppl);
    y = round(y, ppl);
    c = round(c, ppl);


    if (y == 0) {
        if (type == 0) {
            //format is:   ax = c
            if (x < 0) x = -x; else c = -c;

            if (x == 1)
                str += "x = " + c;
            else if (x != 1)
                str += x + "x = " + c;

        }
        else {
            //format is:   x = c/x
            if (x < 0) x = -1; else c = -c / x;
            str += "x = " + c;
        }
    }
    else {
        if ((y == 1) || (y == -1))
            str += "y = ";
        else if ((y > 0) || (y < 0))
            str += y + "y = ";


        if (x == 1)
            str += "x ";
        else if (x == -1)
            str += "-x ";
        else if (x > 0)
            str += x + "x ";
        else if (x < 0)
            str += "-" + Math.abs(x) + "x ";


        if (c == 0)
            str += "";
        else if (c > 0) {
            if (x != 0)
                str += "+ " + c;
            else
                str += c;
        }
        else if (c < 0) {
            if (x != 0)
                str += "- " + Math.abs(c);
            else
                str += c;
        }
    }

    return str;
}

function formatLineNormal(x1, y1, x2, y2, ppl) {
    //return line equation form:     ay + bx + c = 0
    //   inputs two points of the line
    var str = " ";
    
    if ((x1 == undefined) || (x2 == undefined) || (y1 == undefined) | (y2 == undefined))
        return str;
        
    if (x1 == x2) {
        //line parallel to y axis
        return "x = " + round(x1, ppl);
    }
    else if (y1 == y2) {
        //line parallel to x axis
        return "y = " + round(y1, ppl);
    }

    var a = 1;
    var b = round(-(y2 - y1) / (x2 - x1), ppl);
    var c = round(y1 - x1 * (y2 - y1) / (x2 - x1), ppl);

    if (a == 0)
        str = "";
    else if (a == 1)
        str = "y =";
    else if (a == -1)
        str = "-y =";
    else if (a == Infinity)
        return "y = " + b;
    else if (a == -Infinity)
        return "y = -" + Math.abs(b);
    else
        str = " " + a + "y =";


    if (b == 1)
        str += " - x";
    else if (b == -1)
        str += " + x";
    else if (b > 0)
        str += " - " + b + "x";
    else if (b < 0)
        str += " " + Math.abs(b) + "x";
    else if (b == Infinity)
        return "x = -" + a;
    else if (b == -Infinity)
        return "x = +" + Math.abs(a);

    if (c == 0)
        str += "";
    else if (c > 0)
        str += " + " + c;
    else if (c < 0)
        str += " - " + Math.abs(c);

    return str;
}

function formatLinePointSlope(x, y, m, ppl) {
    //return line format:  y - mx - (y1 - m x1) = 0
    //   this is a line passing through point (x1, y1)
    var str = " y ";
    if ((isNaN(x)) || (isNaN(y)) || (isNaN(m)))
        return "";
    else {
        if (isNaN(x)) x1 = 0;
        if (isNaN(y)) y1 = 0;
        if (isNaN(m)) return "";
    }

    if (m == 0) {
        str = " y = " + y;
        return str;
    }
    else if ((m == Infinity) || (m == -Infinity)) {
        str = " x = " + x;
        return str;
    }
    else if (m == 1)
        str += " - x";
    else if (m == -1)
        str += " + x";
    else if (m > 0)
        str += "- " + m + "x";
    else if (m < 0)
        str += "+ " + Math.abs(m) + "x";


    var b = y - m * x;

    if (b == 0)
        str += " = 0";
    else if (b > 0)
        str += " - " + b + " = 0";
    else if (b < 0)
        str += " + " + Math.abs(b) + " = 0";

    return str;
}

//**************************************************************************
//line 3D functions
//
//**************************************************************************

function point3D(x, y, z) { this.x = x; this.y = y; this.z = z; }

function midPoint3D(x1, y1, x2, y2, x3, y3) {
    return new point((x1 + x2) / 2, (y1 + y2) / 2, (z1 + z2) / 2);
}

function pmidPoint3D(p1, p2) {
    return new point((p1.x + p2.x) / 2, (p1.y + p2.y) / 2, (p1.z + p2.z) / 2);
}

function pdistance3D(p1, p2) {
    return Math.sqrt((p1.x - p2.x) * (p1.x - p2.x) + (p1.y - p2.y) * (p1.y - p2.y) + (p1.z - p2.z) * (p1.z - p2.z));
}

function setParamLineEquation(a, at, ppl) {
    //output line equation   a + bt
    var str = "";

    if (isNaN(a) || (a == "")) a = 0;
    if (isNaN(at) || (at == "")) at = 0;
    if ((a == 0) && (at == 0)) return "";

    if (a != 0)
        str += round(a, ppl);

    if (at == 0)
        str += "";
    else if (at == 1) {
        if (str == "")
            str += " t";
        else
            str += " + t";
    }
    else if (at == -1) {
        if (str == "")
            str += " - t";
        else
            str += " - t";
    }
    else if (at < 0)
        str += " - " + round(Math.abs(at), ppl) + "t ";
    else {
        if (a == 0)
            str += round(at, ppl) + "t ";
        else
            str += " + " + round(at, ppl) + "t ";

    }
    return str;
}

function setParamLineEquation(a, at, ppl, stret) {
    //output line equation   a + bt
    //str is the valuereturned whene a=0 and at=0
    var str = "";

    if (isNaN(a) || (a == "")) a = 0;
    if (isNaN(at) || (at == "")) at = 0;
    if ((a == 0) && (at == 0)) return stret;

    if (a != 0)
        str += round(a, ppl);

    if (at == 0)
        str += "";
    else if (at == 1) {
        if (str == "")
            str += " t";
        else
            str += " + t";
    }
    else if (at == -1) {
        if (str == "")
            str += " - t";
        else
            str += " - t";
    }
    else if (at < 0)
        str += " - " + round(Math.abs(at), ppl) + "t ";
    else {
        if (a == 0)
            str += round(at, ppl) + "t ";
        else
            str += " + " + round(at, ppl) + "t ";

    }
    return str;
}

//**************************************************************************
// Plane functions
//
//    Updated  11-25-11
//**************************************************************************

function angleBetween2Planes(a1, b1, c1, a2, b2, c2) {
    // Plane equation   ax + by + cz + d = 0
    //
    // Returns the angle in radian between two planes
    var angAB = Math.abs(a1 * a2 + b1 * b2 + c1 * c2) / (Math.sqrt(a1 * a1 + b1 * b1 + c1 * c1) * Math.sqrt(a2 * a2 + b2 * b2 + c2 * c2))
    if ((Math.abs(angAB - 1) < 0.000000000000001) || (angAB > 1))
        return 0;
    else
        return Math.acos(angAB);
}

function Distance2Lines3D(x1, y1, z1, a1, b1, c1, x2, y2, z2, a2, b2, c2) {
    //calculates the distance between 2 3D lines
    var i;
    var mat = new Array();

    for (i = 0; i < 3; i++) {
        mat[i] = new Array();
    }

    mat[0][0] = x2 - x1;
    mat[0][1] = y2 - y1;
    mat[0][2] = z2 - z1;

    mat[1][0] = a1;
    mat[1][1] = b1;
    mat[1][2] = c1;

    mat[2][0] = a2;
    mat[2][1] = b2;
    mat[2][2] = c2;

    var nom = (b1 * c2 - b2 * c1) * (x2 - x1) - (a1 * c2 - a2 * c1) * (y2 - y1) + (a1 * b2 - a2 * b1) * (z2 - z1);

    var denom = (b1 * c2 - b2 * c1) * (b1 * c2 - b2 * c1);
    denom += (a2 * c1 - c2 * a1) * (a2 * c1 - c2 * a1);
    denom += (b2 * a1 - a2 * b1) * (b2 * a1 - a2 * b1);
    denom = Math.sqrt(denom);
    
    return nom / denom;
}

function formatPlaneEqu(A, B, C, D, ppl, type) {
    //Format plane equation form: 
    //   type = 0 -->   Ax + By + CZ = D
    //   type = 1 -->   Ax + By + CZ + D = 0
    
    var str = "";
    var fac = 1;
    var arr = new Array(A, B, C, D);
    var ret = CommonValueVector0(arr);
    
    A = arr[0];
    B = arr[1];
    C = arr[2];
    D = arr[3];

    if (A < 0) fac = -1;
    A = A * fac; 

    if (A == 0)
        str += "";
    else if (A == 1)
        str += "x ";
    else if (A == -1)
        str += "-x ";
    else
        str += round(A, ppl) + "x ";

    B = B * fac;
    if (B == 0)
        str += "";
    else if (B == 1) {
        if (str != "") str += "+ y "; else str += "y ";
    }
    else if (B == -1)
        str += "- y ";
    else if (B > 0) {
        if (str != "")
            str += "+ " + round(B, ppl) + "y ";
        else
            str += round(B, ppl) + "y ";
    }
    else if (B < 0)
        str += "- " + round(Math.abs(B), ppl) + "y ";

    C = C * fac; 
    if (C == 0)
        str += "";
    else if (C == 1) {
        if (str != "") str += "+ z "; else str += "z ";
    }
    else if (C == -1)
        str += "- z ";
    else if (C > 0) {
        if (str != "")
            str += "+ " + round(C, ppl) + "z ";
        else
            str += round(C, ppl) + "z ";
    }
    else if (C < 0)
        str += "- " + round(Math.abs(C), ppl) + "z ";

    if (type == 0) {
        D = D * fac;
        if (str != "")
            str += "= " + round(D, ppl);
    }
    else {
        if (str != "") {
            if (D == 0)
                str += " = 0";
            else if (D > 0)
                str += "+ " + round(D, ppl) + " = 0";
            else
                str += "- " + Math.abs(round(D, ppl)) + " = 0";
        }
    }

    return str;
}

function formatPlaneEquNorm(A, B, C, D, ppl) {
    //Format plane equation form:   Ax + By + CZ + D = 0
    var str = "";
    var fac = 1;
    var arr = new Array(A, B, C, D);
    var ret = CommonValueVector0(arr);

    A = arr[0];
    B = arr[1];
    C = arr[2];
    D = arr[3];

    if (A < 0) fac = -1;
    A = A * fac;

    if (A == 0)
        str += "";
    else if (A == 1)
        str += "x ";
    else if (A == -1)
        str += "-x ";
    else
        str += round(A, ppl) + "x ";

    B = B * fac;
    if (B == 0)
        str += "";
    else if (B == 1) {
        if (str != "") str += "+ y "; else str += "y ";
    }
    else if (B == -1)
        str += "- y ";
    else if (B > 0) {
        if (str != "")
            str += "+ " + round(B, ppl) + "y ";
        else
            str += round(B, ppl) + "y ";
    }
    else if (B < 0)
        str += "- " + round(Math.abs(B), ppl) + "y ";

    C = C * fac;
    if (C == 0)
        str += "";
    else if (C == 1) {
        if (str != "") str += "+ z "; else str += "z ";
    }
    else if (C == -1)
        str += "- z ";
    else if (C > 0) {
        if (str != "")
            str += "+ " + round(C, ppl) + "z ";
        else
            str += round(C, ppl) + "z ";
    }
    else if (C < 0)
        str += "- " + round(Math.abs(C), ppl) + "z ";

    D = -D * fac;
    if (str != "") {
        if (D == 0)
            str += " = 0";
        else if (D > 0)
            str += "+ " + round(D, ppl) + " = 0";
        else
            str += "- " + Math.abs(round(D, ppl)) + " = 0";
    }

    return str;
}
function isPointsCollinear(mat) {
    //return true  - points are collinear
    //       false - points are not collinear
    // this methode uses the formula of triple equallity.

    var fac0 = mat[1][0] - mat[0][0];
    var fac1 = mat[1][1] - mat[0][1];
    var fac2 = mat[1][2] - mat[0][2];

    var test0 = mat[2][0] - mat[0][0];
    var test1 = mat[2][1] - mat[0][1];
    var test2 = mat[2][2] - mat[0][2];

    var r1 = fac0 * test1 - fac1 * test0;
    var r2 = fac0 * test2 - fac2 * test0;
    var r3 = fac1 * test2 - fac2 * test1;

    if ((r1 == 0) && (r2 == 0) && (r3 == 0)) {
        //Points are collinear
        return true;
    }
    else {
        //Points are NOT collinear
        return false;
    }
}

function isPointsCollinearP(p1, p2, t3) {
    var fac0 = p2.x - p1.x;
    var fac1 = p2.y - p1.y;
    var fac2 = p2.z - p1.z;

    var r1 = fac0 * (t3.y - p1.y) - fac1 * (t3.x - p1.x);
    var r2 = fac0 * (t3.z - t3.z) - fac2 * (t3.x - p1.x);
    var r3 = fac1 * (t3.z - t3.z) - fac2 * (t3.y - p1.y);

    if ((r1 == 0) && (r2 == 0) && (r3 == 0))
        return true;
    else
        return false;
}

function isPlanesCoPlanar(mat) {
    var tmp = new Array();
    tmp[0] = new Array();
    tmp[1] = new Array();
    tmp[2] = new Array();
    
    var n1 = new vec3D(mat[0][0], mat[0][1], mat[0][2]);
    var n2 = new vec3D(mat[1][0], mat[1][1], mat[1][2]);
    var n3 = new vec3D(mat[2][0], mat[2][1], mat[2][2]);

    var cp1 = crossProduct(n1, n2);
    var cp2 = crossProduct(n1, n3);
    var cp3 = crossProduct(n2, n3);

    var dp1 = dotProduct(mat[0][0], mat[0][1], mat[0][2], mat[1][0], mat[1][1], mat[1][2]);
    var dp2 = dotProduct(mat[0][0], mat[0][1], mat[0][2], mat[2][0], mat[2][1], mat[2][2]);
    var dp3 = dotProduct(mat[1][0], mat[1][1], mat[1][2], mat[2][0], mat[2][1], mat[2][2]);
    
    tmp[0][0] = cp1.i; tmp[0][1] = cp1.j; tmp[0][2] = cp1.k;
    tmp[1][0] = cp2.i; tmp[1][1] = cp2.j; tmp[1][2] = cp2.k;
    tmp[2][0] = cp3.i; tmp[2][1] = cp3.j; tmp[2][2] = cp3.k;
    
    if (rank(tmp)==1){
        //the planes are coplaner
        return true;
    }
    else {
        //the planes are not coplaner
        return false
    }
}