from sympy import *

x, y = symbols('x y')
x1, y1 = symbols('x1 y1')
x2, y2 = symbols('x2 y2')
x3, y3 = symbols('x3 y3')
x4, y4 = symbols('x4 y4')

print solve(
	[
		((x - x1) * (y2 - y1)) - ((y - y1) * (x2 - x1)),
		((x - x3) * (y4 - y3)) - ((y - y3) * (x4 - x3))
	], 
	[x, y]
)
