require 'moo_mesh'

module MooObjFile
    class BasePolygon
        attr_accessor :vertices, :triangles, :vertex_indexes

        def initialize(vertices, vertex_indexes = nil)
            @vertices = vertices.dup

            if vertex_indexes.nil?
                @vertex_indexes = (0 .. @vertices.size - 1).to_a
            else
                @vertex_indexes = vertex_indexes.dup
            end

            @triangles = []
        end

        def triangulate
            vertex_indexes = @vertex_indexes.dup
            @triangles = []
            index = 0

            while vertex_indexes.size > 3 and index < vertex_indexes.size
                n = vertex_indexes.size

                if is_vertex_convex? vertex_indexes[index]
                    j, i, t = (index - 1) % n, index, (index + 1) % n

                    @triangles << [ vertex_indexes[j], vertex_indexes[i], vertex_indexes[t] ]

                    vertex_indexes.shift
                end

                index += 1
            end

            @triangles << vertex_indexes

            @triangles
        end

        def area
            s = Vector.new
            n = @vertex_indexes.size

            0.upto(n - 1) do |i|
                j = (i + 1) % n

                s += @vertices[@vertex_indexes[i]].cross_product(@vertices[@vertex_indexes[j]])
            end

            (s / 2.0).length
        end

        def is_vertex_convex?(vertex)
            if vertex.is_a? Numeric
                BasePolygon.new((@vertex_indexes - [ vertex ]).map { |v| @vertices[v] }).area <= self.area
            elsif vertex.is_a? Vector
                new_vertices = @vertex_indexes.map { |v| @vertices[v] }
                new_vertices.delete(vertex)

                BasePolygon.new(new_vertices).area <= self.area
            end
        end
    end

    class ObjFile
        attr_accessor :vertices, :faces, :normals, :chunks
        attr_accessor :triangles

        def initialize
            @chunks = {}
            @vertices, @faces, @trinagles = [], [], []
        end

        def self.open(filename)
            result = ObjFile.new
            result.read(filename)
            result
        end

        def read(filename)
            puts "#{ DateTime.now } > Reading #{ filename } file..."

            @chunks = {}
            @vertices, @faces, @trinagles = [], [], []

            File.open(filename, 'r') do |f|
                while not f.eof?
                    line = f.gets.gsub(/(^\s+)|([\s\n]+$)/, '')

                    matches = line.match(/^(\w+)([\sa-z0-9.\/-]+)*$/)

                    next if matches.nil?

                    command, arguments = matches[1].strip, (matches[2] && matches[2].strip.split(/\s+/))

                    next if command =~ /^\#/ or command.nil? or command.empty?

                    if command == 'v'
                        arguments.map! { |a| a.to_f }
                        @vertices << Vector.new(arguments[0], arguments[1], arguments[2])
                    elsif command == 'f'
                        # vertex indexes start from 1 in OBJ files and from 0 in Moo3D
                        vertex_indexes = arguments.map { |a| a.split(/\//).first.to_i - 1 }

                        @faces << BasePolygon.new(@vertices, vertex_indexes)
                    end

                    @chunks[command] ||= []
                    @chunks[command] << arguments
                end
            end

            puts "#{ DateTime.now } > OBJ file is done reading. Read #{ @faces.size } faces, #{ @vertices.size } vertices."

            @triangles = (@faces.map { |face| face.triangulate }).flatten

            puts "#{ DateTime.now } > Triangulated OBJ geometry. Ended up with #{ @triangles.size / 3 } triangles."
        end

        def write(filename)
            # TODO: implement
        end

        def to_mesh
            Mesh.new(@vertices, @triangles)
        end
    end
end

include MooObjFile