class Numeric
    ETHA = (10.0**-6.0)

    def degrees
        self * Math::PI / 180.0
    end

    def radians
        self * 180.0 / Math::PI
    end

    def =~(v)
        (self - v).abs <= ETHA
    end
end

class Array
    def without(index)
        res = self.dup
        res.delete_at(index)
        res
    end

    def det
        return self[0][0] if self.size == 1

        raise "Wrong matrix element set" unless self[0].is_a? Array

        size = self[0].size

        sign = 1.0
        res = 0.0

        self[0].each_with_index do |itm, idx|
            minor = self.without(0).transpose.without(idx).transpose

            # puts "#{(sign < 0) ? '-' : ''}#{itm} * #{minor.inspect}"

            res += sign * itm * minor.det
            sign *= -1.0
        end

        res
    end
end

module MooMath
    class Vector
        attr_accessor :x, :y, :z

        def initialize(_x = 0.0, _y = 0.0, _z = 0.0)
            @x, @y, @z = _x.to_f, _y.to_f, _z.to_f
            @x = 0.0 if @x =~ Numeric::ETHA
            @y = 0.0 if @y =~ Numeric::ETHA
            @z = 0.0 if @z =~ Numeric::ETHA
        end

        def to_s
            "<#{ @x }, #{ @y }, #{ @z }>"
        end

        def +(v)
            Vector.new (@x + v.x), (@y + v.y), (@z + v.z)
        end

        def -(v)
            Vector.new (@x - v.x), (@y - v.y), (@z - v.z)
        end

        def *(v)
            Vector.new (@x * v), (@y * v), (@z * v)
        end

        def /(v)
            Vector.new (@x / v), (@y / v), (@z / v)
        end

        def ==(v)
            (v - self).length =~ Numeric::ETHA
        end

        def !=(v)
            not ((v - self).length =~ Numeric::ETHA)
        end

        def normalize
            l = self.length
            Vector.new @x / l, @y / l, @z / l
        end

        def length
            Math.sqrt((@x**2.0) + (@y**2.0) + (@z**2.0))
        end

        def angle_to(v)
            Math.acos( (self.dot_product(v)) / (self.length * v.length) )
        end

        def dot_product(v)
            (@x * v.x) + (@y * v.y) + (@z * v.z)
        end

        def cross_product(v)
            Vector.new ((@y * v.z) - (@z * v.y)), ((@z * v.x) - (@x * v.z)), ((@x * v.y) - (@y * v.x))
        end

        def scalar_cross_product(v)
            ((@y * v.z) + (@z * v.x) + (@x * v.y)) - ((@z * v.y) + (@x * v.z) + (@y * v.x))
        end

        def rotate(angle, axis)
            u = axis
            v = self

            ((v - (u * (u.dot_product(v)))) * Math.cos(angle)) +
            ((u.cross_product(v)) * Math.sin(angle)) +
            (u * u.dot_product(v))
        end

        def distance_to(v)
            (v - self).length
        end
    end

    class BaseFigure
        attr_accessor :vertices

        def initialize(*args)
            raise "Unknown figure constructor arguments: #{ args.inspect }" unless args.is_a? Array

            if args.size == 1 and args.first.is_a? Array
                if args.first.first.is_a? Vector
                    @vertices = args.first.dup
                end
            elsif args.size > 1 and args.first.is_a? Vector
                @vertices = args.dup
            end
        end

        def [](index)
            @vertices[index]
        end

        def []=(index, v)
            @vertices[index] = v
        end
    end

    class Triangle < BaseFigure
        attr_accessor :sides

        def initialize(*args)
            super
        end

        def sides
            a, b, c = @vertices[0], @vertices[1], @vertices[2]

            [ Line.new(a, b), Line.new(b, c), Line.new(a, c) ]
        end

        def include?(v, strict = false)
            a, b, c = @vertices[0], @vertices[1], @vertices[2]

            v0 = c - a
            v1 = b - a
            v2 = v - a

            # Compute dot products
            dot00 = v0.dot_product(v0)
            dot01 = v0.dot_product(v1)
            dot02 = v0.dot_product(v2)
            dot11 = v1.dot_product(v1)
            dot12 = v1.dot_product(v2)

            # Compute barycentric coordinates
            invDenom = 1.0 / (dot00 * dot11 - dot01 * dot01)
            u = (dot11 * dot02 - dot01 * dot12) * invDenom
            v = (dot00 * dot12 - dot01 * dot02) * invDenom

            # Check if point is in triangle
            if strict
                (u >= 0.0) && (v >= 0.0) && ((u + v) < 1.0)
            else
                (u > 0.0) && (v > 0.0) && ((u + v) < 1.0)
            end
        end

        def to_s
            "{ #{ ([ @vertices[0], @vertices[1], @vertices[2] ].map { |v| v.to_s }).join ', ' } }"
        end

        alias_method :includes?, :include?

        def ==(v)
            raise "#{ v.inspect } is not a triangle!" unless v.is_a? Triangle

            v.vertices.permutation.to_a.each do |variant|
                return true if (variant[0] == @vertices[0]) and (variant[1] == @vertices[1]) and (variant[2] == @vertices[2])
            end

            false
        end

        def map_onto_plane
            a = @vertices[0].distance_to(@vertices[1])
            b = @vertices[0].distance_to(@vertices[2])
            c = @vertices[2].distance_to(@vertices[1])

            x3 = ((c**2.0) + (a**2.0) - (b**2.0)) / (2.0 * a)

            if x3 > c
                y3 = Math.sqrt((c**2.0) + (x3**2.0))
            else
                y3 = Math.sqrt((c**2.0) - (x3**2.0))
            end

            Triangle.new Vector.new(0, 0), Vector.new(a, 0), Vector.new(x3, y3)
        end

        def map_near_line(l)
            a, b, c = @vertices[0], @vertices[1], @vertices[2]
            ab, bc, ac = b - a, c - b, c - a
            l1 = l.to_vector
            axis = Vector.new 0.0, 0.0, 1.0

            result = []

            if ab.length =~ l1.length
                l = l.reverse if a != l[0]

                alpha = ab.angle_to l1
                result << Triangle.new(l[0], l[1], l[0] + ac.rotate(alpha, axis))

                alpha1 = 180.degrees - alpha
                result << Triangle.new(l[1], l[0], l[1] + ac.rotate(alpha1, axis))

                alpha = 360.degrees - alpha
                result << Triangle.new(l[0], l[1], l[0] + ac.rotate(alpha, axis))

                alpha1 = 180.degrees - alpha
                result << Triangle.new(l[1], l[0], l[1] + ac.rotate(alpha1, axis))
            end

            if bc.length =~ l1.length
                l = l.reverse if b != l[0]

                alpha = bc.angle_to l1
                result << Triangle.new(l[0] + ab.rotate(alpha, axis), l[0], l[1])

                alpha1 = 180.degrees - alpha
                result << Triangle.new(l[1] + ab.rotate(alpha1, axis), l[1], l[0])

                alpha = 360.degrees - alpha
                result << Triangle.new(l[0] + ab.rotate(alpha, axis), l[0], l[1])

                alpha1 = 180.degrees - alpha
                result << Triangle.new(l[1] + ab.rotate(alpha1, axis), l[1], l[0])
            end

            if ac.length =~ l1.length
                l = l.reverse if a != l[0]

                alpha = ac.angle_to l1
                result << Triangle.new(l[0], l[0] + bc.rotate(alpha, axis), l[1])

                alpha1 = 180.degrees - alpha
                result << Triangle.new(l[1], l[1] + bc.rotate(alpha1, axis), l[0])

                alpha = 360.degrees - alpha
                result << Triangle.new(l[0], l[0] + bc.rotate(alpha, axis), l[1])

                alpha1 = 180.degrees - alpha
                result << Triangle.new(l[1], l[1] + bc.rotate(alpha1, axis), l[0])
            end

            result
        end

        def map_onto_plane_near(v)
            if v.is_a? Triangle
                result = []

                (v.vertices.combination(2).map { |pair| map_near_line Line.new(pair) }).compact.each { |e| result += e }

                result.uniq
            elsif v.is_a? Line
                map_near_line(v)
            end
        end

        def overlaps_or_intersects?(v)
            raise "#{ v.inspect } is not a triangle!" unless v.is_a? Triangle

            self.sides.product(v.sides).each do |side_pair|
                return true if side_pair[0].intersect? side_pair[1]
            end

            self.vertices.each do |vertex|
                return true if v.includes? vertex
            end

            false
        end
    end

    class Line < BaseFigure
        def intersect?(v, strict = false)
            # self = (p; p + r) => r = @vertices[1] - p
            p = @vertices[0]
            r = @vertices[1] - p

            q = v[0]
            s = v[1] - q

            return false if r.scalar_cross_product(s) == 0.0

            t = (q - p).scalar_cross_product(s) / (r.scalar_cross_product(s))
            u = (q - p).scalar_cross_product(r) / (r.scalar_cross_product(s))

            p1 = (p + (r * t))
            p2 = (q + (s * u))

            if strict
                p1 == p2
            else
                (p1 == p2) and ((v[1] - p1) == (p1 - v[0])) and ((self[1] - p2) == (p2 - self[0]))
            end
        end

        alias_method :intersects?, :intersect?

        def to_s
            "{ #{ @vertices[0] }, #{ @vertices[1] } }"
        end

        def to_vector
            @vertices[1] - @vertices[0]
        end

        def reverse
            Line.new @vertices.reverse
        end

        def ==(v)
            v.vertices.product(@vertices).select { |p| p[0] == p[1] }.empty?
        end
    end
end

include MooMath