# Welcome

This repository contains my graduation work; a simple 3D texture painting software. 

The idea came from _[Cinema 4D](http://www.maxon.net/)_ and other softwares. The point is, i want to create a **very user-friendly** (unlike _[Blender](http://blender.org/)_) 3D modelling software with **free license** (unlike _[Cinema 4D](http://www.maxon.net/)_ and others) and **trivial process of object painting** (unlike _[3D Studio Max](http://usa.autodesk.com/3ds-max/)_). This repository is the starting point.

Many people may say _Why inventing a wheel?_. Still, many of them have written (for example) bubble sorting algorithm by their own hands. Just to learn something new. The same is here: i want to learn to write such kind of software. It's just fun for me, yet it is a serious project.

**So, what i ask is: be polite, respect yourself and my own; do not be a strict judge on what's happening in the repository, its wiki or source code. Just remember yourself at your student years. Thanks!**

## Dependencies

For so long as it is possible i shall push dependant libraries' sources to the repository.

For now, the software depends on:

* **openassimp**

## Building

[WIP]

* install the libraries required
    * for **Ubuntu Linux**:

            sudo apt-get install libxmu-dev libxi-dev zlib-bin freeglut3-dev
            sudo apt-get install imagemagick libmagickwand-dev
            sudo gem install rmagick

* build **openassimp**
      * for **Ubuntu Linux**:

            cd assimp
            sudo apt-get install cmake-qt-gui
            cmake-gui &>/dev/null &
            # then press "configure" and "generate" (if pressing "configure" did not end up with errors)

**Benchmarks:**

`
    2013-05-16T03:01:18+03:00 > Reading cube.obj file...
    2013-05-16T03:01:18+03:00 > OBJ file is done reading. Read 12 faces, 8 vertices.
    2013-05-16T03:01:18+03:00 > Triangulated OBJ geometry. Ended up with 36 triangles.
    2013-05-16T03:01:18+03:00 > Unwrapping done. Writing unwrapped texture to new_cube.png...
    2013-05-16T03:01:18+03:00 > Generated layers count: 1
    2013-05-16T03:01:19+03:00 > Unwrapping done. Writing unwrapped texture to old_cube.png...
    2013-05-16T03:01:19+03:00 > Generated layers count: 1
    2013-05-16T03:01:19+03:00 > Reading qt.obj file...
    2013-05-16T03:01:19+03:00 > OBJ file is done reading. Read 811 faces, 1644 vertices.
    2013-05-16T03:01:19+03:00 > Triangulated OBJ geometry. Ended up with 4866 triangles.
    2013-05-16T03:05:24+03:00 > Unwrapping done. Writing unwrapped texture to qt.png...
    2013-05-16T03:05:44+03:00 > Generated layers count: 16
`